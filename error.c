#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

void __error(const char *file, const unsigned int line, const char *func, const char *fmt, ...)
{
	fputs("Error: ", stderr);

	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);

	if (errno != 0) {
		fprintf(stderr, ": %s", strerror(errno));
	}

	fprintf(stderr, " (%s:%u '%s')\n", file, line, func);

	exit(EXIT_FAILURE);
}
