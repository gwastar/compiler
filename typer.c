#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include "array.h"
#include "compiler.h"
#include "typer.h"

type_t *type_int;
type_t *type_real;
type_t *type_void;

struct context {
	struct function_declaration *surrounding_function;
};

static void helper(struct ast_node *node, struct context *ctx);

type_t *make_type(enum type_class class)
{
	type_t *type = allocate_memory(sizeof(*type));
	type->class = class;
	return type;
}

// static struct type_name *type_name_from_primitive_type(type_t *type, loc_t loc)
// {
// 	enum primitive_type primitive_type;
// 	if (type == type_int) {
// 		primitive_type = PRIMITIVE_TYPE_INT;
// 	} else {
// 		assert(type == type_real);
// 		primitive_type = PRIMITIVE_TYPE_REAL;
// 	}

// 	struct primitive_type_name *type_name = MAKE_PRIMITIVE_TYPE_NAME(loc.start, loc.end, loc.file);
// 	type_name->primitive_type = primitive_type;
// 	SET_TYPE(type_name, type);
// 	return TYPE_NAME(type_name);
// }

static type_t *infer_type_for_top_level_declaration(struct declaration *decl, struct context *ctx)
{
	assert(!GET_TYPE(decl));

	if (NODE(decl)->class == AST_VARIABLE_DECLARATION) {
		struct variable_declaration *var = VARIABLE_DECLARATION(decl);
		type_t *type;
		if (var->type_name) {
			assert(!var->initializer);
			helper(NODE(var->type_name), ctx);
			type = GET_TYPE(var->type_name);
		} else {
			assert(var->initializer);
			report_error(var, true, "top-level initializers are not supported yet"); // TODO
			// if (NODE(var->initializer)->class != AST_CONSTANT) {
			// 	report_error(var->initializer, true,
			// 		     "top-level initializer must be a compile-time constant value");
			// }
			// struct constant *constant = CONSTANT(var->initializer);
			// helper(NODE(constant), ctx);
			// type = GET_TYPE(var->initializer);
		}
		SET_TYPE(decl, type);
		return type;
	}

	struct function_declaration *func = FUNCTION_DECLARATION(decl);
	type_t *return_type = type_void;
	if (func->return_type_name) {
		// the return type gets validated in the helper below
		helper(NODE(func->return_type_name), ctx);
		return_type = GET_TYPE(func->return_type_name);
	}

	type_t **parameter_types = NULL;
	struct parameter_declaration **pparam;
	array_foreach(func->parameter_declarations, pparam) { // @Cleanup array_foreach_value
		helper(NODE(*pparam), ctx);
		type_t *t = GET_TYPE(*pparam);
		array_add(parameter_types, t);
	}

	type_t *type = make_type(TYPE_FUNCTION);
	type->return_type = return_type;
	type->parameter_types = parameter_types;
	SET_TYPE(decl, type);
	return type;
}

static struct expression *match_type(struct expression *expr, type_t *target_type, struct ast_node *parent,
				     bool fail_silently) {
	type_t *expr_type = GET_TYPE(expr);

	if (target_type == type_real && expr_type == type_int) {
		// only implicit cast in this language
		loc_t loc = NODE(expr)->loc; // @Cleanup mark these as generated?
		// struct type_name *type_name = type_name_from_primitive_type(target_type, loc);
		struct type_name *type_name = NULL;
		struct type_cast *cast = MAKE_TYPE_CAST(loc.start, loc.end, loc.file);
		cast->casted_expression = expr;
		cast->target_type_name = type_name;
		SET_TYPE(cast, type_real);
		return EXPRESSION(cast);
	}

	if (!fail_silently && (target_type->class != TYPE_PRIMITIVE || target_type != expr_type)) {
		report_error(parent, true, "type mismatch"); // TODO better error message
	}

	return expr;
}

static bool can_be_casted_to(type_t *target, type_t *src)
{
	return ((target == type_int  && src == type_int)  ||
		(target == type_int  && src == type_real) ||
		(target == type_real && src == type_int)  ||
		(target == type_real && src == type_real));
}

static bool is_compare_expression(struct binary_expression *binexpr)
{
	switch (binexpr->operator) {
	case BINOP_EQUAL:
	case BINOP_UNEQUAL:
	case BINOP_LESSTHAN:
	case BINOP_LESSEQUAL:
	case BINOP_GREATERTHAN:
	case BINOP_GREATEREQUAL:
		return true;
	default:
		return false;
	}
}

static void helper(struct ast_node *node, struct context *ctx)
{
	if (!node) {
		return;
	}

	switch (node->class) {
	case AST_PROGRAM: {
		struct program *program = PROGRAM(node);
		array_fori(program->declarations, i) {
			infer_type_for_top_level_declaration(program->declarations[i], ctx);
		}

		array_fori(program->declarations, i) {
			helper(NODE(program->declarations[i]), ctx);
		}
		break;
	}
	case AST_BLOCK: {
		struct block *block = BLOCK(node);
		array_fori(block->variable_declarations, i) {
			helper(NODE(block->variable_declarations[i]), ctx);
		}
		array_fori(block->statements, i) {
			helper(NODE(block->statements[i]), ctx);
		}
		break;
	}
	case AST_ARRAY_ACCESS: {
		struct array_access *array_access = ARRAY_ACCESS(node);
		helper(NODE(array_access->array_name), ctx);
		array_fori(array_access->indices, i) {
			helper(NODE(array_access->indices[i]), ctx);
		}
		struct identifier *array_name = array_access->array_name;
		type_t *array_type = GET_TYPE(array_name);
		if (array_type->class != TYPE_ARRAY) {
			report_error(node, true,
				     "identifier '%s' in array access expression does not refer to an array",
				     array_name->name->string);
		}
		size_t array_dim = array_len(array_type->lengths);
		size_t num_indices = array_len(array_access->indices);
		if (num_indices != array_dim) {
			report_error(node, true, "number of indices does not match dimensionality of array: expected '%zu', but got '%zu'", array_dim, num_indices);
		}
		array_fori(array_access->indices, i) {
			struct expression *index = array_access->indices[i];
			if (GET_TYPE(index) != type_int) {
				report_error(index, true, "index '%zu' of array access is not of type 'int'", i);
			}

			if (NODE(index)->class == AST_CONSTANT) {
				uint64_t val = CONSTANT(index)->u64;
				uint64_t len = array_type->lengths[i];
				if (val >= len) {
					report_error(index, true, "index is out of bounds: index is %" PRIu64
						     ", length is %" PRIu64, val, len);
				}
			}
		}
		SET_TYPE(array_access, array_type->base_type);
		break;
	}
	case AST_BINARY_EXPRESSION: {
		struct binary_expression *binary_expression = BINARY_EXPRESSION(node);
		helper(NODE(binary_expression->left_operand), ctx);
		helper(NODE(binary_expression->right_operand), ctx);

		struct expression *lhs = binary_expression->left_operand;
		struct expression *rhs = binary_expression->right_operand;
		type_t *ltype = GET_TYPE(lhs);
		type_t *rtype = GET_TYPE(rhs);
		if (ltype->class != TYPE_PRIMITIVE || ltype->class != TYPE_PRIMITIVE) {
			struct expression *loc = lhs;
			const char *side = "left";
			if (ltype->class == TYPE_PRIMITIVE) {
				loc = rhs;
				side = "right";
			}
			report_error(loc, true, "%s operand does not have a primitive type", side);
		}

		rhs = match_type(rhs, ltype, node, true);
		binary_expression->right_operand = rhs;
		rtype = GET_TYPE(rhs);
		lhs = match_type(lhs, rtype, node, false);
		binary_expression->left_operand = lhs;
		ltype = GET_TYPE(lhs);

		assert(ltype == rtype);

		if (is_compare_expression(binary_expression)) {
			SET_TYPE(binary_expression, type_int);
		} else {
			SET_TYPE(binary_expression, ltype);
		}

		break;
	}
	case AST_CONSTANT: {
		struct constant *constant = CONSTANT(node);
		if (constant->constant_type == CONSTANT_U64) {
			SET_TYPE(constant, type_int);
		} else {
			assert(constant->constant_type == CONSTANT_F64);
			SET_TYPE(constant, type_real);
		}
		break;
	}
	case AST_IDENTIFIER: {
		struct identifier *identifier = IDENTIFIER(node);
		type_t *type = identifier->symbol->type;
		assert(type);
		SET_TYPE(identifier, type);
		break;
	}
	case AST_FUNCTION_CALL: {
		struct function_call *function_call = FUNCTION_CALL(node);
		helper(NODE(function_call->function_name), ctx);
		array_fori(function_call->arguments, i) {
			helper(NODE(function_call->arguments[i]), ctx);
		}

		struct identifier *func_name = function_call->function_name;
		type_t *func_type = GET_TYPE(func_name);
		if (func_type->class != TYPE_FUNCTION) {
			report_error(node, true, "identifier '%s' in function call does not refer to a function",
				     func_name->name->string);
		}

		size_t num_args = array_len(function_call->arguments);
		size_t num_params = array_len(func_type->parameter_types);
		if (num_args != num_params) {
			report_error(node, true,
				     "wrong number of arguments for function call: expected %zu, but got %zu",
				     num_params, num_args);
		}

		array_fori(function_call->arguments, i) {
			type_t *param_type = func_type->parameter_types[i];
			function_call->arguments[i] = match_type(function_call->arguments[i], param_type, node,
								 false);
		}

		SET_TYPE(function_call, func_type->return_type);

		break;
	}
	case AST_TYPE_CAST: {
		struct type_cast *type_cast = TYPE_CAST(node);
		helper(NODE(type_cast->casted_expression), ctx);
		helper(NODE(type_cast->target_type_name), ctx);

		type_t *expr_type = GET_TYPE(type_cast->casted_expression);
		type_t *target_type = GET_TYPE(type_cast->target_type_name);

		if (!can_be_casted_to(target_type, expr_type)) {
			report_error(node, true, "expression cannot be casted to the specified type");
		}

		SET_TYPE(type_cast, target_type);

		break;
	}
	case AST_ASSIGN_STATEMENT: {
		struct assign_statement *assign_statement = ASSIGN_STATEMENT(node);
		helper(NODE(assign_statement->lhs), ctx);
		helper(NODE(assign_statement->rhs), ctx);
		struct expression *lhs = assign_statement->lhs;
		struct expression *rhs = assign_statement->rhs;
		assert(NODE(lhs)->class == AST_IDENTIFIER || NODE(lhs)->class == AST_ARRAY_ACCESS);
		type_t *ltype = GET_TYPE(lhs);
		if (ltype != type_int && ltype != type_real) {
			report_error(lhs, true, "left hand side of assignment must be of type 'int' or 'real'");
		}
		assign_statement->rhs = match_type(rhs, ltype, node, false);
		break;
	}
	case AST_FUNCTION_CALL_STATEMENT: {
		struct function_call_statement *function_call_statement = FUNCTION_CALL_STATEMENT(node);
		helper(NODE(function_call_statement->function_call), ctx);
		break;
	}
	case AST_IF_STATEMENT: {
		struct if_statement *if_statement = IF_STATEMENT(node);
		helper(NODE(if_statement->condition), ctx);
		helper(NODE(if_statement->then_block), ctx);
		helper(NODE(if_statement->else_block), ctx);
		break;
	}
	case AST_RETURN_STATEMENT: {
		struct return_statement *return_statement = RETURN_STATEMENT(node);
		helper(NODE(return_statement->return_value), ctx);

		type_t *return_type = GET_TYPE(ctx->surrounding_function)->return_type;
		struct expression *return_value = return_statement->return_value;
		if (return_value) {
			if (return_type == type_void) {
				report_error(node, true, "return value specified in void function");
			}
			return_statement->return_value = match_type(return_value, return_type, node, false);
		} else {
			if (return_type != type_void) {
				report_error(node, true, "no return value specified in non-void function");
			}
		}
		break;
	}
	case AST_WHILE_STATEMENT: {
		struct while_statement *while_statement = WHILE_STATEMENT(node);
		helper(NODE(while_statement->condition), ctx);
		helper(NODE(while_statement->loop_body), ctx);
		break;
	}
	case AST_ARRAY_TYPE_NAME: {
		struct array_type_name *array_type_name = ARRAY_TYPE_NAME(node);

		assert(!GET_TYPE(array_type_name));

		helper(NODE(array_type_name->base_type_name), ctx);
		type_t *base_type = GET_TYPE(array_type_name->base_type_name);

		uint64_t *lengths = NULL;
		struct expression **pexpr;
		array_foreach(array_type_name->size_expressions, pexpr) {
			if (NODE(*pexpr)->class != AST_CONSTANT) {
				report_error(*pexpr, true,  "array size must be a compile-time constant value");
			}
			struct constant *constant = CONSTANT(*pexpr);
			if (constant->constant_type != CONSTANT_U64) {
				report_error(*pexpr, true,  "array size must be an integer value");
			}
			array_add(lengths, constant->u64);
		}

		type_t *type = make_type(TYPE_ARRAY);
		type->base_type = base_type;
		type->lengths = lengths;
		SET_TYPE(array_type_name, type);
		break;
	}
	case AST_PRIMITIVE_TYPE_NAME: {
		struct primitive_type_name *primitive_type_name = PRIMITIVE_TYPE_NAME(node);
		if (GET_TYPE(primitive_type_name)) {
			break;
		}
		type_t *type;
		if (primitive_type_name->primitive_type == PRIMITIVE_TYPE_INT) {
			type = type_int;
		} else {
			assert(primitive_type_name->primitive_type == PRIMITIVE_TYPE_REAL);
			type = type_real;
		}
		SET_TYPE(primitive_type_name, type);
		break;
	}
	case AST_FUNCTION_DECLARATION: {
		struct function_declaration *function_declaration = FUNCTION_DECLARATION(node);
		type_t *type = GET_TYPE(function_declaration);
		assert(type && type->class == TYPE_FUNCTION);
		if (type->return_type->class != TYPE_PRIMITIVE) {
			report_error(function_declaration->return_type_name, true,
				     "function return type is not a primitive type");
		}

		assert(ctx->surrounding_function == NULL);
		ctx->surrounding_function = function_declaration;
		array_fori(function_declaration->parameter_declarations, i) {
			helper(NODE(function_declaration->parameter_declarations[i]), ctx);
		}
		helper(NODE(function_declaration->return_type_name), ctx);
		helper(NODE(function_declaration->function_body), ctx);
		ctx->surrounding_function = NULL;

		if (strcmp(function_declaration->declaration.name->string, "main") == 0) { // @Cleanup
			if (!array_empty(function_declaration->parameter_declarations)) {
				report_error(node, true, "'main' function must not have any parameters");
			}

			if (type->return_type != type_int) {
				report_error(function_declaration->return_type_name, true,
					     "return type of 'main' function must be 'int'");
			}
		}

		break;
	}
	case AST_PARAMETER_DECLARATION: {
		struct parameter_declaration *parameter_declaration = PARAMETER_DECLARATION(node);
		helper(NODE(parameter_declaration->type_name), ctx);
		type_t *type = GET_TYPE(parameter_declaration->type_name);
		if (type->class != TYPE_PRIMITIVE) {
			report_error(node, true, "function parameter must have a primitive type");
		}
		SET_TYPE(parameter_declaration, type);
		break;
	}
	case AST_VARIABLE_DECLARATION: {
		struct variable_declaration *variable_declaration = VARIABLE_DECLARATION(node);
		if (GET_TYPE(variable_declaration)) {
			break;
		}
		helper(NODE(variable_declaration->type_name), ctx);
		helper(NODE(variable_declaration->initializer), ctx);
		type_t *type;
		if (variable_declaration->type_name) {
			assert(!variable_declaration->initializer);
			type = GET_TYPE(variable_declaration->type_name);
		} else {
			type = GET_TYPE(variable_declaration->initializer);
		}
		SET_TYPE(variable_declaration, type);
		break;
	}
	case AST_INVALID:
	default:
		assert(false);
	}
}

static inline type_t *make_primitive_type(const char *name)
{
	type_t *type = make_type(TYPE_PRIMITIVE);
	type->name = make_atom(name, strlen(name));
	return type;
}

void init_primitive_types(void)
{
	type_int  = make_primitive_type("int");
	type_real = make_primitive_type("real");
	type_void = make_primitive_type("void");
}

void perform_type_checking(struct program *program)
{
	struct context ctx = {
		.surrounding_function = NULL,
	};
	helper(NODE(program), &ctx);
}
