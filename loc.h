#ifndef __LOC_INCLUDE__
#define __LOC_INCLUDE__

#include <stdio.h>

typedef struct source_file {
	const char *path;
	FILE *handle;
	size_t size;
	char buf[]; // after tokenization this contains the null-terminated file data
} srcfile_t;

typedef struct pos {
	size_t line;
	size_t column;
} pos_t;

typedef struct loc {
	srcfile_t *file;
	pos_t start;
	pos_t end;
} loc_t;

static inline loc_t make_loc(srcfile_t *file, size_t start_line, size_t start_column,
			     size_t end_line, size_t end_column)
{
	return (struct loc){file, (pos_t){start_line, start_column}, (pos_t){end_line, end_column}};
}

#endif
