#include <assert.h>
#include <stdio.h>
#include "array.h"
#include "ast.h"
#include "error.h"
#include "parser.h"

struct parser {
	token_t *tokens;
	size_t idx;
};

static struct program *parse_program(struct parser *parser);
static struct block *parse_block(struct parser *parser);
static struct array_access *parse_array_access(struct parser *parser);
static struct binary_expression *parse_binary_expression(struct parser *parser);
static struct constant *parse_constant(struct parser *parser);
static struct identifier *parse_identifier(struct parser *parser);
static struct function_call *parse_function_call(struct parser *parser);
static struct type_cast *parse_type_cast(struct parser *parser);
static struct assign_statement *parse_assign_statement(struct parser *parser);
static struct function_call_statement *parse_function_call_statement(struct parser *parser);
static struct if_statement *parse_if_statement(struct parser *parser);
static struct return_statement *parse_return_statement(struct parser *parser);
static struct while_statement *parse_while_statement(struct parser *parser);
static struct array_type_name *parse_array_type_name(struct parser *parser);
static struct primitive_type_name *parse_primitive_type_name(struct parser *parser);
static struct function_declaration *parse_function_declaration(struct parser *parser);
static struct parameter_declaration *parse_parameter_declaration(struct parser *parser);
static struct variable_declaration *parse_variable_declaration(struct parser *parser);

static struct expression *parse_arithmetic_expression(struct parser *parser);
static struct expression *parse_conditional_expression(struct parser *parser);

static token_t *pop(struct parser *parser)
{
	if (parser->idx >= array_len(parser->tokens)) {
		assert(false);
		return NULL;
	}
	return &parser->tokens[parser->idx++];
}

static token_t *peek(size_t offset, struct parser *parser)
{
	size_t idx = parser->idx + offset;
	if (idx >= array_len(parser->tokens)) {
		idx = array_lasti(parser->tokens);
	}
	return &parser->tokens[idx];
}

static token_t *next(struct parser *parser)
{
	return peek(0, parser);
}

static token_t *pop_expected(enum token_type type, struct parser *parser)
{
	token_t *token = pop(parser);
	if (token->type != type) {
		fprintf(stderr, "%s:%zu:%zu: error: unexpected token\n",
			token->loc.file->path, token->loc.start.line, token->loc.start.column);
		exit(EXIT_FAILURE);
	}
	return token;
}

static struct array_access *parse_array_access(struct parser *parser)
{
	struct identifier *ident = parse_identifier(parser);

	struct expression **indices = NULL;
	token_t *token;
	do {
		pop_expected(TOKEN_OPENBRACKET, parser);
		array_add(indices, parse_arithmetic_expression(parser));
		token = pop_expected(TOKEN_CLOSEBRACKET, parser);
	} while (next(parser)->type == TOKEN_OPENBRACKET);

	pos_t start = NODE(ident)->loc.start;
	pos_t end = token->loc.end;
	struct array_access *access = MAKE_ARRAY_ACCESS(start, end, token->loc.file);
	access->array_name = ident;
	access->indices = indices;
	return access;
}

static struct function_call *parse_function_call(struct parser *parser)
{
	struct identifier *ident = parse_identifier(parser);
	pop_expected(TOKEN_OPENPAREN, parser);

	struct expression **arguments = NULL;
	token_t *token = next(parser);
	if (token->type != TOKEN_CLOSEPAREN) {
		array_add(arguments, parse_arithmetic_expression(parser));
		while (next(parser)->type == TOKEN_COMMA) {
			pop(parser);
			array_add(arguments, parse_arithmetic_expression(parser));
		}
	}
	token = pop_expected(TOKEN_CLOSEPAREN, parser);

	pos_t start = NODE(ident)->loc.start;
	pos_t end = token->loc.end;
	struct function_call *call = MAKE_FUNCTION_CALL(start, end, token->loc.file);
	call->function_name = ident;
	call->arguments = arguments;
	return call;
}

static struct constant *parse_number_literal(struct parser *parser)
{
	token_t *token = pop(parser);
	union {
		uint64_t u64;
		double f64;
	} val;
	enum constant_type type;
	if (token->type == TOKEN_CHARLITERAL) {
		type = CONSTANT_U64;
		val.u64 = token->ch;
	} else if (token->type == TOKEN_INTLITERAL) {
		type = CONSTANT_U64;
		val.u64 = token->u64;
	} else if (token->type == TOKEN_FLOATLITERAL) {
		type = CONSTANT_F64;
		val.f64 = token->f64;
	} else {
		fprintf(stderr, "%s:%zu:%zu: error: unexpected token\n",
			token->loc.file->path, token->loc.start.line, token->loc.start.column);
		exit(EXIT_FAILURE);
	}

	struct constant *constant = MAKE_CONSTANT(token->loc.start, token->loc.end, token->loc.file);
	constant->constant_type = type;
	constant->u64 = val.u64;
	return constant;
}

static struct expression *parse_factor_expression(struct parser *parser)
{
	if (next(parser)->type == TOKEN_INTLITERAL ||
	    next(parser)->type == TOKEN_FLOATLITERAL ||
	    next(parser)->type == TOKEN_CHARLITERAL) {
		return &parse_number_literal(parser)->expression;
	}

	if (next(parser)->type == TOKEN_OPENPAREN) {
		token_t *token = pop(parser);
		pos_t start = token->loc.start;
		struct expression *expr = parse_arithmetic_expression(parser);
		if (next(parser)->type != TOKEN_AS) {
			pop_expected(TOKEN_CLOSEPAREN, parser);
			return expr;
		}

		pop(parser);
		struct primitive_type_name *type_name = parse_primitive_type_name(parser);

		token = pop_expected(TOKEN_CLOSEPAREN, parser);
		pos_t end = token->loc.end;
		srcfile_t *file = token->loc.file;
		struct type_cast *cast = MAKE_TYPE_CAST(start, end, file);
		cast->casted_expression = expr;
		cast->target_type_name = &type_name->type_name;
		return &cast->expression;
	}

	token_t *token = peek(1, parser);
	if (token->type == TOKEN_OPENPAREN) {
		return &parse_function_call(parser)->expression;
	} else if (token->type == TOKEN_OPENBRACKET) {
		return &parse_array_access(parser)->expression;
	} else {
		return &parse_identifier(parser)->expression;
	}
}

static struct expression *parse_multiplicative_expression(struct parser *parser)
{
	struct expression *lhs = parse_factor_expression(parser);

	for (;;) {
		token_t *token = next(parser);
		if (token->type != TOKEN_ASTERISK && token->type != TOKEN_SLASH) {
			return lhs;
		}
		pop(parser);
		struct expression *rhs = parse_factor_expression(parser);

		pos_t start = NODE(lhs)->loc.start;
		pos_t end = NODE(rhs)->loc.end;
		srcfile_t *file = NODE(rhs)->loc.file;
		struct binary_expression *binexpr = MAKE_BINARY_EXPRESSION(start, end, file);
		binexpr->left_operand = lhs;
		binexpr->right_operand = rhs;
		binexpr->operator = token->type == TOKEN_ASTERISK ? BINOP_MUL : BINOP_DIV;
		lhs = EXPRESSION(binexpr);
	}
}

static struct expression *parse_additive_expression(struct parser *parser)
{
	struct expression *lhs = parse_multiplicative_expression(parser);
	for (;;) {
		token_t *token = next(parser);
		if (token->type != TOKEN_PLUS && token->type != TOKEN_MINUS) {
			return lhs;
		}
		pop(parser);
		struct expression *rhs = parse_multiplicative_expression(parser);

		pos_t start = NODE(lhs)->loc.start;
		pos_t end = NODE(rhs)->loc.end;
		srcfile_t *file = NODE(rhs)->loc.file;
		struct binary_expression *binexpr = MAKE_BINARY_EXPRESSION(start, end, file);
		binexpr->left_operand = lhs;
		binexpr->right_operand = rhs;
		binexpr->operator = token->type == TOKEN_PLUS ? BINOP_PLUS : BINOP_MINUS;
		lhs = EXPRESSION(binexpr);
	}
}

static struct expression *parse_arithmetic_expression(struct parser *parser)
{
	return parse_additive_expression(parser);
}

static struct expression *parse_compare_expression(struct parser *parser)
{
	token_t *token = next(parser);
	if (token->type == TOKEN_OPENPAREN) {
		// TODO this ( could also belong to an arithmetic expression!!!
		pop(parser);
		struct expression *expr = parse_conditional_expression(parser);
		pop_expected(TOKEN_CLOSEPAREN, parser);
		return expr;
	}

	struct expression *lhs = parse_arithmetic_expression(parser);

	token = pop(parser);
	enum binary_operator binop;
	switch (token->type) {
	case TOKEN_EQUAL:        binop = BINOP_EQUAL; break;
	case TOKEN_UNEQUAL:      binop = BINOP_UNEQUAL; break;
	case TOKEN_LESSTHAN:     binop = BINOP_LESSTHAN; break;
	case TOKEN_LESSEQUAL:    binop = BINOP_LESSEQUAL; break;
	case TOKEN_GREATERTHAN:  binop = BINOP_GREATERTHAN; break;
	case TOKEN_GREATEREQUAL: binop = BINOP_GREATEREQUAL; break;
	default:
		fprintf(stderr, "%s:%zu:%zu: error: unexpected token\n",
			token->loc.file->path, token->loc.start.line, token->loc.start.column);
		exit(EXIT_FAILURE);
	}

	struct expression *rhs = parse_arithmetic_expression(parser);

	pos_t start = NODE(lhs)->loc.start;
	pos_t end = NODE(rhs)->loc.end;
	srcfile_t *file = NODE(rhs)->loc.file;
	struct binary_expression *binexpr = MAKE_BINARY_EXPRESSION(start, end, file);
	binexpr->left_operand = lhs;
	binexpr->right_operand = rhs;
	binexpr->operator = binop;
	return &binexpr->expression;
}

static struct expression *parse_and_expression(struct parser *parser)
{
	struct expression *expr = parse_compare_expression(parser);
	token_t *token = next(parser);
	if (token->type != TOKEN_AND) {
		return expr;
	}

	pop(parser);
	struct expression *lhs = expr;
	struct expression *rhs = parse_and_expression(parser);

	pos_t start = NODE(lhs)->loc.start;
	pos_t end = NODE(rhs)->loc.end;
	srcfile_t *file = NODE(rhs)->loc.file;
	struct binary_expression *binexpr = MAKE_BINARY_EXPRESSION(start, end, file);
	binexpr->left_operand = lhs;
	binexpr->right_operand = rhs;
	binexpr->operator = BINOP_AND;
	return &binexpr->expression;
}

static struct expression *parse_or_expression(struct parser *parser)
{
	struct expression *expr = parse_and_expression(parser);
	token_t *token = next(parser);
	if (token->type != TOKEN_OR) {
		return expr;
	}

	pop(parser);
	struct expression *lhs = expr;
	struct expression *rhs = parse_or_expression(parser);

	pos_t start = NODE(lhs)->loc.start;
	pos_t end = NODE(rhs)->loc.end;
	srcfile_t *file = NODE(rhs)->loc.file;
	struct binary_expression *binexpr = MAKE_BINARY_EXPRESSION(start, end, file);
	binexpr->left_operand = lhs;
	binexpr->right_operand = rhs;
	binexpr->operator = BINOP_OR;
	return &binexpr->expression;
}

static struct expression *parse_conditional_expression(struct parser *parser)
{
	return parse_or_expression(parser);
}

static struct expression *parse_lvalue(struct parser *parser)
{
	token_t *token = peek(1, parser);
	if (token->type == TOKEN_OPENBRACKET) {
		return &parse_array_access(parser)->expression;
	} else {
		return &parse_identifier(parser)->expression;
	}
}

static struct identifier *parse_identifier(struct parser *parser)
{
	token_t *token = pop_expected(TOKEN_IDENT, parser);
	pos_t start = token->loc.start;
	pos_t end = token->loc.end;
	struct identifier *ident = MAKE_IDENTIFIER(start, end, token->loc.file);
	ident->name = token->atom;
	return ident;
}

static struct return_statement *parse_return_statement(struct parser *parser)
{
	token_t *token = pop(parser);
	assert(token->type == TOKEN_RETURN);
	pos_t start = token->loc.start;

	struct expression *retval = NULL;
	token = next(parser);
	if (token->type != TOKEN_SEMICOLON) {
		retval = parse_arithmetic_expression(parser);
	}

	token = pop_expected(TOKEN_SEMICOLON, parser);
	pos_t end = token->loc.end;
	struct return_statement *statement = MAKE_RETURN_STATEMENT(start, end, token->loc.file);
	statement->return_value = retval;
	return statement;
}

static struct while_statement *parse_while_statement(struct parser *parser)
{
	token_t *token = pop(parser);
	assert(token->type == TOKEN_WHILE);
	pos_t start = token->loc.start;
	struct expression *condition = parse_conditional_expression(parser);
	pop_expected(TOKEN_DO, parser);
	struct block *body = parse_block(parser);

	token = pop_expected(TOKEN_END, parser);
	pos_t end = token->loc.end;
	struct while_statement *statement = MAKE_WHILE_STATEMENT(start, end, token->loc.file);
	statement->condition = condition;
	statement->loop_body = body;
	return statement;
}

static struct if_statement *parse_if_statement(struct parser *parser)
{
	token_t *token = pop(parser);
	assert(token->type == TOKEN_IF);
	pos_t start = token->loc.start;
	struct expression *condition = parse_conditional_expression(parser);
	pop_expected(TOKEN_THEN, parser);
	struct block *then_block = parse_block(parser);

	struct block *else_block = NULL;
	token = next(parser);
	if (token->type == TOKEN_ELSE) {
		pop(parser);
		else_block = parse_block(parser);
	}

	token = pop_expected(TOKEN_END, parser);
	pos_t end = token->loc.end;
	struct if_statement *statement = MAKE_IF_STATEMENT(start, end, token->loc.file);
	statement->condition = condition;
	statement->then_block = then_block;
	statement->else_block = else_block;
	return statement;
}

static struct assign_statement *parse_assign_statement(struct parser *parser)
{
	struct expression *lhs = parse_lvalue(parser);
	pop_expected(TOKEN_ASSIGN, parser);
	struct expression *rhs = parse_arithmetic_expression(parser);
	token_t *token = pop_expected(TOKEN_SEMICOLON, parser);
	pos_t start = NODE(lhs)->loc.start;
	pos_t end = token->loc.end;
	struct assign_statement *statement = MAKE_ASSIGN_STATEMENT(start, end, token->loc.file);
	statement->lhs = lhs;
	statement->rhs = rhs;
	return statement;
}

static struct function_call_statement *parse_function_call_statement(struct parser *parser)
{
	struct function_call *call = parse_function_call(parser);
	token_t *token = pop_expected(TOKEN_SEMICOLON, parser);
	pos_t start = NODE(call)->loc.start;
	pos_t end = token->loc.end;
	struct function_call_statement *statement = MAKE_FUNCTION_CALL_STATEMENT(start, end,
										 token->loc.file);
	statement->function_call = call;
	return statement;
}

static struct statement *parse_statement(struct parser *parser)
{
	token_t *token = next(parser);
	if (token->type == TOKEN_IF) { // @Cleanup
		return &parse_if_statement(parser)->statement;
	} else if (token->type == TOKEN_WHILE) {
		return &parse_while_statement(parser)->statement;
	} else if (token->type == TOKEN_RETURN) {
		return &parse_return_statement(parser)->statement;
	} else if (token->type == TOKEN_IDENT) {
		// @Cleanup
		token = peek(1, parser);
		if (token->type == TOKEN_OPENPAREN) {
			return &parse_function_call_statement(parser)->statement;
		}
		return &parse_assign_statement(parser)->statement;
	} else {
		fprintf(stderr, "%s:%zu:%zu: error: unexpected token\n",
			token->loc.file->path, token->loc.start.line, token->loc.start.column);
		exit(EXIT_FAILURE);
	}
}

static struct block *parse_block(struct parser *parser)
{
	pos_t start = next(parser)->loc.start; // is this fine for empty blocks?
	struct variable_declaration **decls = NULL;
	struct statement **statements = NULL;
	while (next(parser)->type != TOKEN_END && next(parser)->type != TOKEN_ELSE) { // @Cleanup ?
		if (next(parser)->type == TOKEN_VAR) {
			array_add(decls, parse_variable_declaration(parser));
		} else {
			array_add(statements, parse_statement(parser));
		}
	}

	token_t *token = next(parser);
	pos_t end = token->loc.end; // this is a little inconsistent...
	struct block *block = MAKE_BLOCK(start, end, token->loc.file);
	block->variable_declarations = decls;
	block->statements = statements;
	return block;
}

static struct primitive_type_name *parse_primitive_type_name(struct parser *parser)
{
	token_t *token = pop(parser);
	enum primitive_type primitive_type;
	if (token->type == TOKEN_INT) {
		primitive_type = PRIMITIVE_TYPE_INT;
	} else if (token->type == TOKEN_REAL) {
		primitive_type = PRIMITIVE_TYPE_REAL;
	} else {
		fprintf(stderr, "%s:%zu:%zu: error: unexpected token\n",
			token->loc.file->path, token->loc.start.line, token->loc.start.column);
		exit(EXIT_FAILURE);
	}

	struct primitive_type_name *type_name = MAKE_PRIMITIVE_TYPE_NAME(token->loc.start, token->loc.end,
									 token->loc.file);
	type_name->primitive_type = primitive_type;
	return type_name;
}

static struct type_name *parse_type_name(struct parser *parser)
{
	struct primitive_type_name *primitive = parse_primitive_type_name(parser);

	token_t *token = next(parser);
	if (token->type != TOKEN_OPENBRACKET) {
		return &primitive->type_name;
	}

	struct expression **size_expressions = NULL;
	do {
		pop(parser);
		struct expression *expr = parse_arithmetic_expression(parser);
		token = pop_expected(TOKEN_CLOSEBRACKET, parser);
		array_add(size_expressions, expr);
	} while (next(parser)->type == TOKEN_OPENBRACKET);

	pos_t start = NODE(primitive)->loc.start;
	pos_t end = token->loc.end;
	struct array_type_name *array = MAKE_ARRAY_TYPE_NAME(start, end, token->loc.file);
	array->base_type_name = &primitive->type_name;
	array->size_expressions = size_expressions;
	return &array->type_name;
}

static struct parameter_declaration *parse_parameter_declaration(struct parser *parser)
{
	token_t *token = pop_expected(TOKEN_IDENT, parser);
	pop_expected(TOKEN_COLON, parser);
	struct type_name *type_name = parse_type_name(parser);

	pos_t start = token->loc.start;
	pos_t end = NODE(type_name)->loc.end;
	srcfile_t *file = token->loc.file;
	struct parameter_declaration *decl = MAKE_PARAMETER_DECLARATION(start, end, file);
	decl->declaration.name = token->atom;
	decl->type_name = type_name;
	return decl;
}

static struct variable_declaration *parse_variable_declaration(struct parser *parser)
{
	token_t *token = pop(parser);
	assert(token->type == TOKEN_VAR);
	pos_t start = token->loc.start;

	token = pop_expected(TOKEN_IDENT, parser);
	atom_t *name = token->atom;

	struct type_name *type_name = NULL;
	struct expression *initializer = NULL;
	if (next(parser)->type == TOKEN_COLON) {
		token = pop(parser);
		type_name = parse_type_name(parser);
	} else {
		token = pop_expected(TOKEN_ASSIGN, parser);
		initializer = parse_arithmetic_expression(parser);
	}

	token = pop_expected(TOKEN_SEMICOLON, parser);
	pos_t end = token->loc.end;

	struct variable_declaration *decl = MAKE_VARIABLE_DECLARATION(start, end, token->loc.file);
	decl->declaration.name = name;
	decl->type_name = type_name;
	decl->initializer = initializer;
	return decl;
}

static struct function_declaration *parse_function_declaration(struct parser *parser)
{
	token_t *token = pop(parser);
	assert(token->type == TOKEN_FUNC);
	pos_t start = token->loc.start;

	token = pop_expected(TOKEN_IDENT, parser);
	atom_t *name = token->atom;
	token = pop_expected(TOKEN_OPENPAREN, parser);

	struct parameter_declaration **parameters = NULL;
	token = next(parser);
	if (token->type != TOKEN_CLOSEPAREN) {
		array_add(parameters, parse_parameter_declaration(parser));
		for (;;) {
			token = next(parser);
			if (token->type != TOKEN_COMMA) {
				break;
			}
			pop(parser);
			array_add(parameters, parse_parameter_declaration(parser));
		}
	}
	token = pop_expected(TOKEN_CLOSEPAREN, parser);

	struct type_name *return_type_name = NULL;
	token = next(parser);
	if (token->type == TOKEN_COLON) {
		pop(parser);
		return_type_name = parse_type_name(parser);
	}

	struct block *body = parse_block(parser);

	token = pop_expected(TOKEN_END, parser);
	pos_t end = token->loc.end;

	struct function_declaration *decl = MAKE_FUNCTION_DECLARATION(start, end, token->loc.file);
	decl->declaration.name = name;
	decl->parameter_declarations = parameters;
	decl->return_type_name = return_type_name;
	decl->function_body = body;
	return decl;
}

static struct program *parse_program(struct parser *parser)
{
	if (array_empty(parser->tokens)) {
		return NULL;
	}

	token_t *token = next(parser);
	pos_t start = token->loc.start;
	struct declaration **decls = NULL;
	while (token->type != TOKEN_EOF) {
		struct declaration *decl;
		if (token->type == TOKEN_VAR) { // @Cleanup
			decl = (struct declaration *)parse_variable_declaration(parser);
		} else if (token->type == TOKEN_FUNC) {
			decl = (struct declaration *)parse_function_declaration(parser);
		} else {
			fprintf(stderr, "%s:%zu:%zu: error: unexpected token at top level\n",
				token->loc.file->path, token->loc.start.line, token->loc.start.column);
			exit(EXIT_FAILURE);
		}
		array_add(decls, decl);

		token = next(parser);
	}
	token = pop(parser);
	pos_t end = token->loc.end;

	struct program *program = MAKE_PROGRAM(start, end, token->loc.file);
	program->declarations = decls;
	return program;
}

struct program *parse(token_t *tokens)
{
	struct parser parser = {
		.tokens = tokens,
		.idx = 0,
	};

	struct program *program = parse_program(&parser);
	assert(parser.idx == array_len(tokens));
	return program;
}
