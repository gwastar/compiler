#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "array.h"
#include "ast.h"
#include "atom.h"
#include "backend_x64.h"
#include "compiler.h"
#include "constant_folding.h"
#include "error.h"
#include "ir.h"
#include "mprintf.h"
#include "name_analysis.h"
#include "parser.h"
#include "tokenizer.h"

#define MEM_ARENA_DEFAULT_SIZE (4 * 4096)

struct __membucket {
	struct __membucket *next;
	size_t used;
	size_t capacity;
	char mem[];
};

struct mem_arena {
	struct __membucket *first;
	struct __membucket *last;
};

static void mem_arena_init(struct mem_arena *arena)
{
	arena->first = NULL;
	arena->last = NULL;
}

static void mem_arena_destroy(struct mem_arena *arena)
{
	struct __membucket *cur = arena->first;
	while (cur) {
		struct __membucket *next = cur->next;
		free(cur);
		cur = next;
	}
	mem_arena_init(arena);
}

static void *mem_arena_alloc(struct mem_arena *arena, size_t size)
{
	struct __membucket *bucket = arena->last;
	if (bucket && (bucket->capacity - bucket->used >= size)) {
		void *result = &bucket->mem[bucket->used];
		bucket->used += size;
		return result;
	}

	// if size is greater than MEM_ARENA_DEFAULT_SIZE the bucket is immediatly full...
	size_t capacity = size > MEM_ARENA_DEFAULT_SIZE ? size : MEM_ARENA_DEFAULT_SIZE;
	bucket = malloc(sizeof(*bucket) + capacity);
	bucket->next = NULL;
	bucket->used = size;
	bucket->capacity = capacity;

	if (arena->last) {
		arena->last->next = bucket;
	}
	arena->last = bucket;

	return bucket->mem;
}

static struct mem_arena global_arena;

void *allocate_memory(size_t size)
{
	void *mem = mem_arena_alloc(&global_arena, size);
	memset(mem, 0, size);
	return mem;
}

#define RED "\033[31m"
#define GREEN "\033[32m"
#define RESET "\033[0m"

static void print_context(loc_t loc)
{
	char *cursor = loc.file->buf;
	size_t line, col;
	for (line = 1; line < loc.start.line; line++) {
		cursor = strchr(cursor, '\n') + 1;
	}

	fprintf(stderr, "%8zu |", line);
	for (col = 1; col < loc.start.column; col++) {
		fputc(*cursor, stderr);
		cursor++;
	}
	fputs(GREEN, stderr);
	do  {
		fputc(*cursor, stderr);
		if (*cursor == '\n') {
			fputs(RESET, stderr);
			fprintf(stderr, "         |");
			fputs(GREEN, stderr);
			line++;
			col = 1;
		} else {
			col++;
		}
		cursor++;
	} while (line != loc.end.line || col <= loc.end.column);
	fputs(RESET, stderr);
	while (*cursor != '\0' && *cursor != '\n') {
		fputc(*cursor, stderr);
		cursor++;
	}
	fputc('\n', stderr);
}

void _report_error(loc_t loc, bool show_context, const char *fmt, ...)
{
	fprintf(stderr, "%s:%zu:%zu: " RED "error: " RESET, loc.file->path, loc.start.line, loc.start.column);
	va_list args;
	va_start(args, fmt);
	size_t n = vfprintf(stderr, fmt, args);
	va_end(args);
	fputc('\n', stderr);

	if (show_context) {
		print_context(loc);
	}

	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	mem_arena_init(&global_arena);

	bool stop_before_assembler = false;
	bool stop_before_linker = false;
	bool keep_intermediate_files = false;
	char *output_file_name = NULL;
	for (;;) {
		int rv = getopt(argc, argv, "o:kSc");
		if (rv == -1) {
			break;
		}
		switch (rv) {
		case '?':
			fprintf(stderr, "Usage: %s [-k] [-S] [-c] [-o <output>] <input>\n", argv[0]);
			return 1;
		case 'o':
			output_file_name = optarg;
			break;
		case 'k':
			keep_intermediate_files = true;
			break;
		case 'S':
			stop_before_assembler = true;
			break;
		case 'c':
			stop_before_linker = true;
			break;
		}
	}

	error_on(optind >= argc, "no input file");

	const char **object_file_names = NULL;
	for (int i = optind; i < argc; i++) {
		char *input_file_name = argv[i];
		char *extension = strrchr(input_file_name, '.');
		if (!extension || strcmp(extension, ".e2") != 0) {
			error("file extension '%s' not recognized", extension);
		}
		char *file_name = strndup(input_file_name, extension - input_file_name);
		if (!output_file_name) {
			output_file_name = file_name;
		}

		init_global_atom_table();
		init_primitive_types();

		token_t *tokens = tokenize(input_file_name);
		//dump_tokens(tokens);

		struct program *program = parse(tokens);
		// dump_ast(NODE(program));

		perform_name_analysis(program);
		perform_constant_folding(program);
		perform_type_checking(program);
		// dump_ast(NODE(program));

		struct ir_program *ir_program = generate_ir(program);
		// dump_ir(ir_program);

		char *assembler_code_file_name = stop_before_assembler ? output_file_name : file_name;
		assembler_code_file_name = mprintf("%s.%s", assembler_code_file_name,
						   get_file_extension_assembler_code());
		generate_code(ir_program, assembler_code_file_name);

		if (stop_before_assembler) {
			continue;
		}

		char *object_file_name = stop_before_linker ? output_file_name : file_name;
		object_file_name = mprintf("%s.%s", object_file_name, get_file_extension_object_file());

		run_assembler(assembler_code_file_name, object_file_name);

		if (!keep_intermediate_files) {
			int rv = remove(assembler_code_file_name);
			if (rv == -1) {
				fprintf(stderr, "unable to remove intermediate file '%s'",
					assembler_code_file_name);
			}
		}

		if (!stop_before_linker) {
			array_add(object_file_names, object_file_name);
		}
	}

	if (stop_before_assembler || stop_before_linker) {
		return 0;
	}

	run_linker(object_file_names, output_file_name);

	if (!keep_intermediate_files) {
		const char **object_file_name; // @Cleanup array_foreach_value
		array_foreach(object_file_names, object_file_name) {
			int rv = remove(*object_file_name);
			if (rv == -1) {
				fprintf(stderr, "unable to remove intermediate file '%s'", *object_file_name);
			}
		}
	}

	return 0;
}
