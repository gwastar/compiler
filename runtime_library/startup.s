.intel_syntax noprefix

.extern main
.extern exit

.text

.global _start
_start:
    call main
    push rax
    call exit
