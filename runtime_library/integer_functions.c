#include <stddef.h>
#include <stdbool.h>
#include <inttypes.h>

#define isdigit(c) (((c) >= '0') && ((c) <= '9'))

extern int64_t __readChar(void);
extern int64_t __writeChar(int64_t c);

int64_t __readInt(void)
{
	int64_t c;
	do {
		c = __readChar();
	} while (!isdigit(c) && c != '-');

	bool negative = false;
	uint64_t val = c - '0';
	if (c == '-') {
		negative = true;
		val = 0;
	}

	for (;;) {
		c = __readChar();
		if (!isdigit(c)) {
			break;
		}
		val *= 10;
		val += c - '0';
	}

	return negative ? -val : val;
}

int64_t __writeInt(int64_t val)
{
	char buf[32];
	size_t len = 0;
	uint64_t uval = val;

	bool negative = false;
	if (val < 0) {
		negative = true;
		uval = -val;
	}

	do {
		buf[len++] = '0' + (uval % 10);
		uval /= 10;
	} while (uval);

	if (negative) {
		buf[len++] = '-';
	}

	for (size_t i = 0; i < len; i++) {
		if (__writeChar(buf[len - i - 1]) != 1) {
			return 0;
		}
	}
	return len;
}
