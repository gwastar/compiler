.intel_syntax noprefix


.text

.global readChar
readChar:
    push rbp
    mov rbp, rsp
    sub rsp, 1
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    syscall
    xor rax, rax
    mov	al, [rsp]
    leave
    ret

.global __readChar
__readChar:
    jmp readChar

.global writeChar
writeChar:
    mov rdi, 1
    lea rsi, [rsp + 8]
    mov rdx, 1
    mov rax, 1
    syscall
    cmp rax, 1
    xor rcx, rcx
    cmovne rax, rcx
    ret

.global __writeChar
__writeChar:
    push rdi
    call writeChar
    add rsp, 8
    ret

.extern __readInt
.global readInt
readInt:
    jmp __readInt

.extern __writeInt
.global writeInt
writeInt:
    mov rdi, [rsp + 8]
    jmp __writeInt

.global time
time:
    # set up base pointer
    push rbp
    mov rbp, rsp

    # reserve stack space for 'timeval' struct
    sub rsp, 16

    # 'gettimeofday' syscall
    lea rdi, [rbp - 16]  # pointer to the 'timeval' struct
    mov rsi, 0           # pointer to the 'timezone' struct (not needed)
    mov rax, 96          # syscall number 96 -> gettimeofday
    syscall

    mov rdi, [rbp - 16]  # get seconds
    mov rax, [rbp - 8]   # get microseconds

    # compute time in milliseconds (== seconds*1000 + microseconds/1000)
    imul rdi, 1000
    cqto
    movq rcx, 1000
    idiv rcx
    addq rax, rdi       # rax (return register) now contains time in milliseconds

    # restore old base pointer and return
    leave
    ret

.global exit
exit:
    mov rdi, [rsp + 8]
    mov rax, 60
    syscall
