#ifndef __TOKENIZER_INCLUDE__
#define __TOKENIZER_INCLUDE__

#include <stdint.h>
#include "atom.h"
#include "compiler.h"

enum token_type {
	TOKEN_INVALID,
	TOKEN_EOF,

	TOKEN_CHARLITERAL,
	TOKEN_INTLITERAL,
	TOKEN_FLOATLITERAL,

	TOKEN_IDENT,
	TOKEN_VAR,
	TOKEN_FUNC,
	TOKEN_END,
	TOKEN_IF,
	TOKEN_THEN,
	TOKEN_ELSE,
	TOKEN_WHILE,
	TOKEN_DO,
	TOKEN_RETURN,
	TOKEN_OR,
	TOKEN_AND,
	TOKEN_AS,
	TOKEN_INT,
	TOKEN_REAL,

	TOKEN_EQUAL,
	TOKEN_UNEQUAL,
	TOKEN_LESSTHAN,
	TOKEN_LESSEQUAL,
	TOKEN_GREATERTHAN,
	TOKEN_GREATEREQUAL,
	TOKEN_ASSIGN, // :=
	TOKEN_SEMICOLON = ';',
	TOKEN_COLON = ':',
	TOKEN_COMMA = ',',
	TOKEN_OPENPAREN = '(',
	TOKEN_CLOSEPAREN = ')',
	TOKEN_OPENBRACKET = '[',
	TOKEN_CLOSEBRACKET = ']',
	TOKEN_PLUS = '+',
	TOKEN_MINUS = '-',
	TOKEN_ASTERISK = '*',
	TOKEN_SLASH = '/',
};

typedef struct token {
	struct loc loc;
	enum token_type type;
	union {
		atom_t *atom;
		uint64_t u64;
		double f64;
		uint8_t ch;
	};
} token_t;

token_t *tokenize(const char *filepath);
void dump_tokens(token_t *tokens);

#endif
