#include <string.h>
#include "atom.h"
#include "compiler.h"
#include "hashtable.h"

static bool atoms_match(const atom_t *a, const atom_t *b)
{
	if (a->hash != b->hash ||
	    a->len != b->len) {
		return false;
	}
	return memcmp(a->string, b->string, a->len) == 0;
}

DEFINE_HASHSET(atom_table, atom_t *, 8, atoms_match(*a, *b))

static struct atom_table atom_table;

static uint64_t string_hash(const void *string, size_t len)
{
	const void *data = string;
	const size_t nbytes = len;
	if (data == NULL || nbytes == 0) {
		return 0;
	}

	const uint32_t c1 = 0xcc9e2d51;
	const uint32_t c2 = 0x1b873593;

	const int nblocks = nbytes / 4;
	const uint32_t *blocks = (const uint32_t *)data;
	const uint8_t *tail = (const uint8_t *)data + (nblocks * 4);

	uint32_t h = 0;

	int i;
	uint32_t k;
	for (i = 0; i < nblocks; i++) {
		k = blocks[i];

		k *= c1;
		k = (k << 15) | (k >> (32 - 15));
		k *= c2;

		h ^= k;
		h = (h << 13) | (h >> (32 - 13));
		h = (h * 5) + 0xe6546b64;
	}

	k = 0;
	switch (nbytes & 3) {
	case 3:
		k ^= tail[2] << 16;
	case 2:
		k ^= tail[1] << 8;
	case 1:
		k ^= tail[0];
		k *= c1;
		k = (k << 15) | (k >> (32 - 15));
		k *= c2;
		h ^= k;
	};

	h ^= nbytes;

	h ^= h >> 16;
	h *= 0x85ebca6b;
	h ^= h >> 13;
	h *= 0xc2b2ae35;
	h ^= h >> 16;

	return h;
}

void init_global_atom_table(void)
{
	atom_table_init(&atom_table, 256);
}

static atom_t *find_atom(const char *name, size_t len, uint64_t hash)
{
	atom_t atom = {
		.string = name,
		.len = len,
		.hash = hash,
	};
	atom_t **found = atom_table_lookup(&atom_table, &atom, atom.hash);
	return found ? *found : NULL;
}

atom_t *make_atom(const char *name, size_t len)
{
	uint64_t hash = string_hash(name, len);
	atom_t *atom = find_atom(name, len, hash);
	if (atom) {
		return atom;
	}
	atom = allocate_memory(sizeof(*atom));
	atom->string = name;
	atom->len = len;
	atom->hash = hash;
	return *atom_table_insert(&atom_table, atom, hash);
}
