#ifndef __ERROR_INCLUDE__
#define __ERROR_INCLUDE__

#define error_on(expr, ...) (!(expr) ? (void)0 : error(__VA_ARGS__))
#define error(...) __error(__FILE__, __LINE__, __func__, __VA_ARGS__)

void __attribute__ ((format (printf, 4, 5)))
__error(const char *file, const unsigned int line, const char *func, const char *fmt, ...);

#endif
