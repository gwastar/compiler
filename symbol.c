#include "array.h"
#include "compiler.h"
#include "symbol.h"

DEFINE_HASHMAP(scope_table, atom_t *, symbol_t *, 8, *a == *b)

symbol_t *make_symbol(atom_t *name, struct declaration *decl, enum symbol_class class)
{
	symbol_t *symbol = allocate_memory(sizeof(*symbol));
	symbol->name = name;
	symbol->declaration = decl;
	symbol->class = class;
	return symbol;
}

void symbol_table_init(struct symbol_table *table)
{
	table->scope_stack = NULL;
}

void symbol_table_destroy(struct symbol_table *table)
{
	assert(array_empty(table->scope_stack));
	array_free(table->scope_stack);
}

void enter_scope(struct symbol_table *table)
{
	scope_table_init(array_addn(table->scope_stack, 1), 32);
}

void leave_scope(struct symbol_table *table)
{
	scope_table_destroy(&array_last(table->scope_stack));
	(void)array_pop(table->scope_stack);
}

static unsigned int hash(atom_t *atom)
{
	// TODO return atom->hash?
	uintptr_t h = (uintptr_t)atom;
	if (sizeof(uintptr_t) > sizeof(int)) {
		h = h ^ (h >> (8 * sizeof(int)));
	}
	return (unsigned int)h;
}

bool put_symbol(struct symbol_table *table, symbol_t *symbol)
{
	struct scope_table *scope = &array_last(table->scope_stack);
	symbol_t **s = scope_table_lookup(scope, symbol->name, hash(symbol->name));
	if (s) {
		return false;
	}
	s = scope_table_insert(scope, symbol->name, hash(symbol->name));
	*s = symbol;
	return true;
}

symbol_t *lookup_symbol(struct symbol_table *table, atom_t *name)
{
	struct scope_table *scope;
	array_foreach(table->scope_stack, scope) {
		symbol_t **s = scope_table_lookup(scope, name, hash(name));
		if (s) {
			return *s;
		}
	}

	return NULL;
}
