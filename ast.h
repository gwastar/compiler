#ifndef __AST_INCLUDE__
#define __AST_INCLUDE__

#include "atom.h"
#include "loc.h"
#include "symbol.h"
#include "typer.h"

enum ast_node_class {
	AST_INVALID,

	AST_PROGRAM,
	AST_BLOCK,

	AST_ARRAY_ACCESS,
	AST_BINARY_EXPRESSION,
	AST_CONSTANT,
	AST_IDENTIFIER,
	AST_FUNCTION_CALL,
	AST_TYPE_CAST,

	AST_ASSIGN_STATEMENT,
	AST_FUNCTION_CALL_STATEMENT,
	AST_IF_STATEMENT,
	AST_RETURN_STATEMENT,
	AST_WHILE_STATEMENT,

	AST_ARRAY_TYPE_NAME,
	AST_PRIMITIVE_TYPE_NAME,

	AST_FUNCTION_DECLARATION,
	AST_PARAMETER_DECLARATION,
	AST_VARIABLE_DECLARATION,
};

struct ast_node {
	enum ast_node_class class;
	size_t serial;
	loc_t loc;
};

struct expression {
	struct ast_node node;
	type_t *type;
};

struct statement {
	struct ast_node node;
};

struct declaration {
	struct ast_node node;
	atom_t *name;
	symbol_t *symbol;
};

struct type_name {
	struct ast_node node;
	type_t *type;
};

struct program {
	struct ast_node node;
	struct declaration **declarations;
};

struct block {
	struct ast_node node;
	struct variable_declaration **variable_declarations;
	struct statement **statements;
};

struct array_access {
	struct expression expression;
	struct identifier *array_name;
	struct expression **indices;
};

struct binary_expression {
	struct expression expression;
	struct expression *left_operand;
	struct expression *right_operand;
	enum binary_operator {
		BINOP_INVALID,
		BINOP_PLUS,
		BINOP_MINUS,
		BINOP_MUL,
		BINOP_DIV,
		BINOP_OR,
		BINOP_AND,
		BINOP_EQUAL,
		BINOP_UNEQUAL,
		BINOP_LESSTHAN,
		BINOP_LESSEQUAL,
		BINOP_GREATERTHAN,
		BINOP_GREATEREQUAL,
	} operator;
};

struct constant {
	struct expression expression;
	enum constant_type {
		CONSTANT_INVALID,
		CONSTANT_U64,
		CONSTANT_F64,
	} constant_type;
	union {
		uint64_t u64;
		double f64;
	};
};

struct identifier {
	struct expression expression;
	atom_t *name;
	symbol_t *symbol;
};

struct function_call {
	struct expression expression;
	struct identifier *function_name;
	struct expression **arguments;
};

struct type_cast {
	struct expression expression;
	struct expression *casted_expression;
	struct type_name *target_type_name;
};

struct assign_statement {
	struct statement statement;
	struct expression *lhs;
	struct expression *rhs;
};

struct function_call_statement {
	struct statement statement;
	struct function_call *function_call;
};

struct if_statement {
	struct statement statement;
	struct expression *condition;
	struct block *then_block;
	struct block *else_block;
};

struct return_statement {
	struct statement statement;
	struct expression *return_value;
};

struct while_statement {
	struct statement statement;
	struct expression *condition;
	struct block *loop_body;
};

struct array_type_name {
	struct type_name type_name;
	struct type_name *base_type_name;
	struct expression **size_expressions;
};

struct primitive_type_name {
	struct type_name type_name;
	enum primitive_type {
		PRIMITIVE_TYPE_INVALID,
		PRIMITIVE_TYPE_INT,
		PRIMITIVE_TYPE_REAL,
	} primitive_type;
};

struct function_declaration {
	struct declaration declaration;
	struct parameter_declaration **parameter_declarations;
	struct type_name *return_type_name;
	struct block *function_body;
};

struct parameter_declaration {
	struct declaration declaration;
	struct type_name *type_name;
};

struct variable_declaration {
	struct declaration declaration;
	struct type_name *type_name;
	struct expression *initializer;
};

void dump_ast(struct ast_node *ast);

struct ast_node *__make_ast_node(enum ast_node_class class, size_t size, loc_t loc);
#define MAKE_AST_NODE(type_name, class, startpos, endpos, filepath) \
	((struct type_name *)__make_ast_node(class, sizeof(struct type_name), \
					     make_loc(filepath, startpos.line, startpos.column, \
						      endpos.line, endpos.column)))
#define MAKE_PROGRAM(startpos, endpos, filepath)			\
	MAKE_AST_NODE(program, AST_PROGRAM, startpos, endpos, filepath)
#define MAKE_BLOCK(startpos, endpos, filepath)				\
	MAKE_AST_NODE(block, AST_BLOCK, startpos, endpos, filepath)
#define MAKE_ARRAY_ACCESS(startpos, endpos, filepath)			\
	MAKE_AST_NODE(array_access, AST_ARRAY_ACCESS, startpos, endpos, filepath)
#define MAKE_BINARY_EXPRESSION(startpos, endpos, filepath)		\
	MAKE_AST_NODE(binary_expression, AST_BINARY_EXPRESSION, startpos, endpos, filepath)
#define MAKE_CONSTANT(startpos, endpos, filepath)			\
	MAKE_AST_NODE(constant, AST_CONSTANT, startpos, endpos, filepath)
#define MAKE_IDENTIFIER(startpos, endpos, filepath)			\
	MAKE_AST_NODE(identifier, AST_IDENTIFIER, startpos, endpos, filepath)
#define MAKE_FUNCTION_CALL(startpos, endpos, filepath)			\
	MAKE_AST_NODE(function_call, AST_FUNCTION_CALL, startpos, endpos, filepath)
#define MAKE_TYPE_CAST(startpos, endpos, filepath)			\
	MAKE_AST_NODE(type_cast, AST_TYPE_CAST, startpos, endpos, filepath)
#define MAKE_ASSIGN_STATEMENT(startpos, endpos, filepath)		\
	MAKE_AST_NODE(assign_statement, AST_ASSIGN_STATEMENT, startpos, endpos, filepath)
#define MAKE_FUNCTION_CALL_STATEMENT(startpos, endpos, filepath)	\
	MAKE_AST_NODE(function_call_statement, AST_FUNCTION_CALL_STATEMENT, startpos, endpos, filepath)
#define MAKE_IF_STATEMENT(startpos, endpos, filepath)			\
	MAKE_AST_NODE(if_statement, AST_IF_STATEMENT, startpos, endpos, filepath)
#define MAKE_RETURN_STATEMENT(startpos, endpos, filepath)		\
	MAKE_AST_NODE(return_statement, AST_RETURN_STATEMENT, startpos, endpos, filepath)
#define MAKE_WHILE_STATEMENT(startpos, endpos, filepath)		\
	MAKE_AST_NODE(while_statement, AST_WHILE_STATEMENT, startpos, endpos, filepath)
#define MAKE_ARRAY_TYPE_NAME(startpos, endpos, filepath)		\
	MAKE_AST_NODE(array_type_name, AST_ARRAY_TYPE_NAME, startpos, endpos, filepath)
#define MAKE_PRIMITIVE_TYPE_NAME(startpos, endpos, filepath)		\
	MAKE_AST_NODE(primitive_type_name, AST_PRIMITIVE_TYPE_NAME, startpos, endpos, filepath)
#define MAKE_FUNCTION_DECLARATION(startpos, endpos, filepath)		\
	MAKE_AST_NODE(function_declaration, AST_FUNCTION_DECLARATION, startpos, endpos, filepath)
#define MAKE_PARAMETER_DECLARATION(startpos, endpos, filepath)		\
	MAKE_AST_NODE(parameter_declaration, AST_PARAMETER_DECLARATION, startpos, endpos, filepath)
#define MAKE_VARIABLE_DECLARATION(startpos, endpos, filepath)		\
	MAKE_AST_NODE(variable_declaration, AST_VARIABLE_DECLARATION, startpos, endpos, filepath)

#define _IS_EXPRESSION(CLASS) ((CLASS) == AST_ARRAY_ACCESS ||		\
			       (CLASS) == AST_BINARY_EXPRESSION ||	\
			       (CLASS) == AST_CONSTANT ||		\
			       (CLASS) == AST_IDENTIFIER ||		\
			       (CLASS) == AST_FUNCTION_CALL ||		\
			       (CLASS) == AST_TYPE_CAST)

#define _IS_STATEMENT(CLASS) ((CLASS) == AST_ASSIGN_STATEMENT ||	\
			      (CLASS) == AST_FUNCTION_CALL_STATEMENT ||	\
			      (CLASS) == AST_IF_STATEMENT ||		\
			      (CLASS) == AST_RETURN_STATEMENT ||	\
			      (CLASS) == AST_WHILE_STATEMENT)

#define _IS_TYPE_NAME(CLASS) ((CLASS) == AST_ARRAY_TYPE_NAME ||		\
			      (CLASS) == AST_PRIMITIVE_TYPE_NAME)

#define _IS_DECLARATION(CLASS) ((CLASS) == AST_FUNCTION_DECLARATION ||	\
				(CLASS) == AST_PARAMETER_DECLARATION ||	\
				(CLASS) == AST_VARIABLE_DECLARATION)

#define IS_EXPRESSION(N)						\
	_Generic((N),							\
		 struct array_access *: true,				\
		 struct binary_expression *: true,			\
		 struct constant *: true,				\
		 struct identifier *: true,				\
		 struct function_call *: true,				\
		 struct type_cast *: true,				\
		 struct ast_node *: _IS_EXPRESSION(((struct ast_node *)(N))->class), \
		 enum ast_node_class: _IS_EXPRESSION((enum ast_node_class)(N)) \
		)

#define IS_STATEMENT(N)							\
	_Generic((N),							\
		 struct assign_statement *: true,			\
		 struct function_call_statement *: true,		\
		 struct if_statement *: true,				\
		 struct return_statement *: true,			\
		 struct while_statement *: true,			\
		 struct ast_node *: _IS_STATEMENT(((struct ast_node *)(N))->class), \
		 enum ast_node_class: _IS_STATEMENT((enum ast_node_class)(N)) \
		)

#define IS_TYPE_NAME(N)							\
	_Generic((N),							\
		 struct array_type_name *: true,			\
		 struct primitive_type_name *: true,			\
		 struct ast_node *: _IS_TYPE_NAME(((struct ast_node *)(N))->class), \
		 enum ast_node_class: _IS_TYPE_NAME((enum ast_node_class)(N)) \
		)

#define IS_DECLARATION(N)						\
	_Generic((N),							\
		 struct function_declaration *: true,			\
		 struct parameter_declaration *: true,			\
		 struct variable_declaration *: true,			\
		 struct ast_node *: _IS_DECLARATION(((struct ast_node *)(N))->class), \
		 enum ast_node_class: _IS_DECLARATION((enum ast_node_class)(N)) \
		)

#define EXPRESSION(N)							\
	_Generic((N),							\
		 struct array_access *: &((struct array_access *)(N))->expression, \
		 struct binary_expression *: &((struct binary_expression *)(N))->expression, \
		 struct constant *: &((struct constant *)(N))->expression, \
		 struct identifier *: &((struct identifier *)(N))->expression, \
		 struct function_call *: &((struct function_call *)(N))->expression, \
		 struct type_cast *: &((struct type_cast *)(N))->expression, \
		 struct expression *: (struct expression *)(N),		\
		 struct ast_node *: (assert(IS_EXPRESSION((struct ast_node *)(N))), (struct expression *)(N)) \
		)

#define STATEMENT(N)							\
	_Generic((N),							\
		 struct assign_statement *: &((struct assign_statement *)(N))->statement, \
		 struct function_call_statement *: &((struct function_call_statement *)(N))->statement, \
		 struct if_statement *: &((struct if_statement *)(N))->statement, \
		 struct return_statement *: &((struct return_statement *)(N))->statement, \
		 struct while_statement *: &((struct while_statement *)(N))->statement, \
		 struct statement *: (struct statement *)(N),		\
		 struct ast_node *: (assert(IS_STATEMENT((struct ast_node *)(N))), (struct statement *)(N)) \
		)

#define TYPE_NAME(N)							\
	_Generic((N),							\
		 struct array_type_name *: &((struct array_type_name *)(N))->type_name, \
		 struct primitive_type_name *: &((struct primitive_type_name *)(N))->type_name, \
		 struct type_name *: (struct type_name *)(N),		\
		 struct ast_node *: (assert(IS_TYPE_NAME((struct ast_node *)(N))), (struct type_name *)(N)) \
		)

#define DECLARATION(N)							\
	_Generic((N),							\
		 struct function_declaration *: &((struct function_declaration *)(N))->declaration, \
		 struct parameter_declaration *: &((struct parameter_declaration *)(N))->declaration, \
		 struct variable_declaration *: &((struct variable_declaration *)(N))->declaration, \
		 struct declaration *: (struct declaration *)(N),		\
		 struct ast_node *: (assert(IS_DECLARATION((struct ast_node *)(N))), (struct declaration *)(N)) \
		)

#define NODE(N)								\
	_Generic((N),							\
		 struct expression *: &((struct expression *)(N))->node, \
		 struct statement *: &((struct statement *)(N))->node,	\
		 struct declaration *: &((struct declaration *)(N))->node, \
		 struct type_name *: &((struct type_name *)(N))->node,	\
		 struct program *: &((struct program *)(N))->node,	\
		 struct block *: &((struct block *)(N))->node,		\
		 struct array_access *: &((struct array_access *)(N))->expression.node, \
		 struct binary_expression *: &((struct binary_expression *)(N))->expression.node, \
		 struct constant *: &((struct constant *)(N))->expression.node,	\
		 struct identifier *: &((struct identifier *)(N))->expression.node, \
		 struct function_call *: &((struct function_call *)(N))->expression.node, \
		 struct type_cast *: &((struct type_cast *)(N))->expression.node, \
		 struct assign_statement *: &((struct assign_statement *)(N))->statement.node, \
		 struct function_call_statement *: &((struct function_call_statement *)(N))->statement.node, \
		 struct if_statement *: &((struct if_statement *)(N))->statement.node, \
		 struct return_statement *: &((struct return_statement *)(N))->statement.node, \
		 struct while_statement *: &((struct while_statement *)(N))->statement.node, \
		 struct array_type_name *: &((struct array_type_name *)(N))->type_name.node, \
		 struct primitive_type_name *: &((struct array_type_name *)(N))->type_name.node, \
		 struct function_declaration *: &((struct function_declaration *)(N))->declaration.node, \
		 struct parameter_declaration *: &((struct parameter_declaration *)(N))->declaration.node, \
		 struct variable_declaration *: &((struct variable_declaration *)(N))->declaration.node, \
		 struct ast_node *: (N)					\
		)

#define PROGRAM(N) (assert(NODE(N)->class == AST_PROGRAM), (struct program *)(N))
#define BLOCK(N) (assert(NODE(N)->class == AST_BLOCK), (struct block *)(N))
#define ARRAY_ACCESS(N) (assert(NODE(N)->class == AST_ARRAY_ACCESS), (struct array_access *)(N))
#define BINARY_EXPRESSION(N) (assert(NODE(N)->class == AST_BINARY_EXPRESSION), (struct binary_expression *)(N))
#define CONSTANT(N) (assert(NODE(N)->class == AST_CONSTANT), (struct constant *)(N))
#define IDENTIFIER(N) (assert(NODE(N)->class == AST_IDENTIFIER), (struct identifier *)(N))
#define FUNCTION_CALL(N) (assert(NODE(N)->class == AST_FUNCTION_CALL), (struct function_call *)(N))
#define TYPE_CAST(N) (assert(NODE(N)->class == AST_TYPE_CAST), (struct type_cast *)(N))
#define ASSIGN_STATEMENT(N) (assert(NODE(N)->class == AST_ASSIGN_STATEMENT), (struct assign_statement *)(N))
#define FUNCTION_CALL_STATEMENT(N) (assert(NODE(N)->class == AST_FUNCTION_CALL_STATEMENT), (struct function_call_statement *)(N))
#define IF_STATEMENT(N) (assert(NODE(N)->class == AST_IF_STATEMENT), (struct if_statement *)(N))
#define RETURN_STATEMENT(N) (assert(NODE(N)->class == AST_RETURN_STATEMENT), (struct return_statement *)(N))
#define WHILE_STATEMENT(N) (assert(NODE(N)->class == AST_WHILE_STATEMENT), (struct while_statement *)(N))
#define ARRAY_TYPE_NAME(N) (assert(NODE(N)->class == AST_ARRAY_TYPE_NAME), (struct array_type_name *)(N))
#define PRIMITIVE_TYPE_NAME(N) (assert(NODE(N)->class == AST_PRIMITIVE_TYPE_NAME), (struct primitive_type_name *)(N))
#define FUNCTION_DECLARATION(N) (assert(NODE(N)->class == AST_FUNCTION_DECLARATION), (struct function_declaration *)(N))
#define PARAMETER_DECLARATION(N) (assert(NODE(N)->class == AST_PARAMETER_DECLARATION), (struct parameter_declaration *)(N))
#define VARIABLE_DECLARATION(N) (assert(NODE(N)->class == AST_VARIABLE_DECLARATION), (struct variable_declaration *)(N))

#define _TYPE_PTR(N)							\
	_Generic((N),							\
		 struct expression *: &EXPRESSION((struct expression *)(N))->type, \
		 struct array_access *: &EXPRESSION((struct array_access *)(N))->type, \
		 struct binary_expression *: &EXPRESSION((struct binary_expression *)(N))->type, \
		 struct constant *: &EXPRESSION((struct constant *)(N))->type, \
		 struct identifier *: &EXPRESSION((struct identifier *)(N))->type, \
		 struct function_call *: &EXPRESSION((struct function_call *)(N))->type, \
		 struct type_cast *: &EXPRESSION((struct type_cast *)(N))->type, \
		 struct type_name *: &TYPE_NAME((struct type_name *)(N))->type, \
		 struct array_type_name *: &TYPE_NAME((struct array_type_name *)(N))->type, \
		 struct primitive_type_name *: &TYPE_NAME((struct primitive_type_name *)(N))->type, \
		 struct declaration *: &DECLARATION((struct declaration *)(N))->symbol->type, \
		 struct function_declaration *: &DECLARATION((struct function_declaration *)(N))->symbol->type, \
		 struct parameter_declaration *: &DECLARATION((struct parameter_declaration *)(N))->symbol->type, \
		 struct variable_declaration *: &DECLARATION((struct variable_declaration *)(N))->symbol->type)

#define GET_TYPE(N) (*_TYPE_PTR(N))
#define SET_TYPE(N, T) (*_TYPE_PTR(N) = (T))

#define _SYMBOL_PTR(N)							\
	_Generic((N),							\
		 struct identifier *: &((struct identifier *)(N))->symbol, \
		 struct declaration *: &DECLARATION((struct declaration *)(N))->symbol, \
		 struct function_declaration *: &DECLARATION((struct function_declaration *)(N))->symbol, \
		 struct parameter_declaration *: &DECLARATION((struct parameter_declaration *)(N))->symbol, \
		 struct variable_declaration *: &DECLARATION((struct variable_declaration *)(N))->symbol)

#define GET_SYMBOL(N) (*_SYMBOL_PTR(N))
#define SET_SYMBOL(N, S) (*_SYMBOL_PTR(N) = (S))


#endif
