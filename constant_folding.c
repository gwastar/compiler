#include "array.h"
#include "constant_folding.h"

static struct expression *fold(struct binary_expression *bin_expr) {
	struct expression *left = bin_expr->left_operand;
	struct expression *right = bin_expr->right_operand;

	struct constant *left_int = NULL;
	struct constant *right_int = NULL;
	if (left->node.class == AST_CONSTANT) {
		struct constant *lconst = CONSTANT(left);
		if (lconst->constant_type == CONSTANT_U64) {
			left_int = lconst;
		}
	}
	if (right->node.class == AST_CONSTANT) {
		struct constant *rconst = CONSTANT(right);
		if (rconst->constant_type == CONSTANT_U64) {
			right_int = rconst;
		}
	}

	if (left_int && right_int) {
		uint64_t result;
		uint64_t lval = left_int->u64;
		uint64_t rval = right_int->u64;
		switch (bin_expr->operator) {
		case BINOP_PLUS:
			result = lval + rval;
			break;
		case BINOP_MINUS:
			result = lval - rval;
			break;
		case BINOP_MUL:
			result = lval * rval;
			break;
		case BINOP_DIV:
			if (rval != 0) {
				result = lval / rval;
				break;
			}
			// fallthrough
		default:
			return EXPRESSION(bin_expr);
		}
		pos_t start = left->node.loc.start;
		pos_t end = right->node.loc.end;
		srcfile_t *file = right->node.loc.file;
		struct constant *substitution = MAKE_CONSTANT(start, end, file);
		substitution->constant_type = CONSTANT_U64;
		substitution->u64 = result;
		return EXPRESSION(substitution);
	}

	if (left_int) {
		uint64_t lval = left_int->u64;
		switch (bin_expr->operator) {
		case BINOP_PLUS:
			if (lval == 0) {
				return right;
			}
			break;
		case BINOP_MUL:
			if (lval == 1) {
				return right;
			}
			break;
		default: break;
		}
	}

	if (right_int) {
		uint64_t rval = right_int->u64;
		switch (bin_expr->operator) {
		case BINOP_PLUS:
			if (rval == 0) {
				return left;
			}
			break;
		case BINOP_MINUS:
			if (rval == 0) {
				return left;
			}
			break;
		case BINOP_MUL:
			if (rval == 1) {
				return left;
			}
			break;
		case BINOP_DIV:
			if (rval == 1) {
				return left;
			}
			break;
		default: break;
		}
	}

	return EXPRESSION(bin_expr);
}

static struct expression *helper(struct ast_node *node)
{
	if (!node) {
		return NULL;
	}
	struct expression *retval = NULL;
	switch (node->class) {
	case AST_PROGRAM: {
		struct program *program = PROGRAM(node);
		array_fori(program->declarations, i) {
			helper(NODE(program->declarations[i]));
		}
		break;
	}
	case AST_BLOCK: {
		struct block *block = BLOCK(node);
		array_fori(block->variable_declarations, i) {
			helper(NODE(block->variable_declarations[i]));
		}
		array_fori(block->statements, i) {
			helper(NODE(block->statements[i]));
		}
		break;
	}
	case AST_ARRAY_ACCESS: {
		struct array_access *array_access = ARRAY_ACCESS(node);
		helper(NODE(array_access->array_name));
		array_fori(array_access->indices, i) {
			struct expression *subst = helper(NODE(array_access->indices[i]));
			array_access->indices[i] = subst;
		}
		retval = EXPRESSION(array_access);
		break;
	}
	case AST_BINARY_EXPRESSION: {
		struct binary_expression *binary_expression = BINARY_EXPRESSION(node);
		struct expression *subst = helper(NODE(binary_expression->left_operand));
		binary_expression->left_operand = subst;
		subst = helper(NODE(binary_expression->right_operand));
		binary_expression->right_operand = subst;
		retval = fold(binary_expression);
		break;
	}
	case AST_CONSTANT: {
		struct constant *constant = CONSTANT(node);
		retval = EXPRESSION(constant);
		break;
	}
	case AST_IDENTIFIER: {
		struct identifier *identifier = IDENTIFIER(node);
		retval = EXPRESSION(identifier);
		break;
	}
	case AST_FUNCTION_CALL: {
		struct function_call *function_call = FUNCTION_CALL(node);
		helper(NODE(function_call->function_name));
		array_fori(function_call->arguments, i) {
			struct expression *subst = helper(NODE(function_call->arguments[i]));
			function_call->arguments[i] = subst;
		}
		retval = EXPRESSION(function_call);
		break;
	}
	case AST_TYPE_CAST: {
		struct type_cast *type_cast = TYPE_CAST(node);
		struct expression *subst = helper(NODE(type_cast->casted_expression));
		type_cast->casted_expression = subst;
		helper(NODE(type_cast->target_type_name));
		retval = EXPRESSION(type_cast);
		break;
	}
	case AST_ASSIGN_STATEMENT: {
		struct assign_statement *assign_statement = ASSIGN_STATEMENT(node);
		struct expression *subst = helper(NODE(assign_statement->lhs));
		assert(subst == assign_statement->lhs); // lvalues cannot be constant expressions
		subst = helper(NODE(assign_statement->rhs));
		assign_statement->rhs = subst;
		break;
	}
	case AST_FUNCTION_CALL_STATEMENT: {
		struct function_call_statement *function_call_statement = FUNCTION_CALL_STATEMENT(node);
		helper(NODE(function_call_statement->function_call));
		break;
	}
	case AST_IF_STATEMENT: {
		struct if_statement *if_statement = IF_STATEMENT(node);
		struct expression *subst = helper(NODE(if_statement->condition));
		if_statement->condition = subst;
		helper(NODE(if_statement->then_block));
		helper(NODE(if_statement->else_block));
		break;
	}
	case AST_RETURN_STATEMENT: {
		struct return_statement *return_statement = RETURN_STATEMENT(node);
		struct expression *subst = helper(NODE(return_statement->return_value));
		return_statement->return_value = subst;
		break;
	}
	case AST_WHILE_STATEMENT: {
		struct while_statement *while_statement = WHILE_STATEMENT(node);
		struct expression *subst = helper(NODE(while_statement->condition));
		while_statement->condition = subst;
		helper(NODE(while_statement->loop_body));
		break;
	}
	case AST_ARRAY_TYPE_NAME: {
		struct array_type_name *array_type_name = ARRAY_TYPE_NAME(node);
		helper(NODE(array_type_name->base_type_name));
		array_fori(array_type_name->size_expressions, i) {
			struct expression *subst = helper(NODE(array_type_name->size_expressions[i]));
			array_type_name->size_expressions[i] = subst;
		}
		break;
	}
	case AST_PRIMITIVE_TYPE_NAME: {
		struct primitive_type_name *primitive_type_name = PRIMITIVE_TYPE_NAME(node);
		break;
	}
	case AST_FUNCTION_DECLARATION: {
		struct function_declaration *function_declaration = FUNCTION_DECLARATION(node);
		array_fori(function_declaration->parameter_declarations, i) {
			helper(NODE(function_declaration->parameter_declarations[i]));
		}
		helper(NODE(function_declaration->return_type_name));
		helper(NODE(function_declaration->function_body));
		break;
	}
	case AST_PARAMETER_DECLARATION: {
		struct parameter_declaration *parameter_declaration = PARAMETER_DECLARATION(node);
		helper(NODE(parameter_declaration->type_name));
		break;
	}
	case AST_VARIABLE_DECLARATION: {
		struct variable_declaration *variable_declaration = VARIABLE_DECLARATION(node);
		helper(NODE(variable_declaration->type_name));
		struct expression *subst = helper(NODE(variable_declaration->initializer));
		variable_declaration->initializer = subst;
		break;
	}
	case AST_INVALID:
	default:
		assert(false);
	}

	return retval;
}

void perform_constant_folding(struct program *program)
{
	helper(NODE(program));
}
