.PHONY: all debug test clean

CC ?= clang
VERBOSE ?= @
PATH_PREFIX ?= .
DBGDIR ?= ./debug
OBJDIR ?= $(PATH_PREFIX)/build
DEPDIR ?= $(PATH_PREFIX)/dep

WARNFLAGS = -Wall -pedantic -Wno-unused-function -Wno-unused-parameter -Wno-unused-variable
OPT ?= -O3
OPTFLAGS = $(OPT)
CCFLAGS = $(OPTFLAGS) $(WARNFLAGS) -g -pthread

C_SOURCES = $(shell find . -name "*.c" -and ! -name '.*' -and ! -wholename '*/runtime_library/*' )
C_OBJECTS = $(notdir $(C_SOURCES:.c=.o))
BINARIES = $(notdir $(C_SOURCES:.c=))
DEP_FILES = $(patsubst ./%.c,$(DEPDIR)/%.d,$(C_SOURCES))
OBJPRE = $(addprefix $(OBJDIR)/, $(C_OBJECTS))
BINARY = $(PATH_PREFIX)/e2c

all: $(BINARY) $(MAKEFILE_LIST)
	$(VERBOSE) $(MAKE) -C runtime_library/ OPT=$(OPT)

debug:
	@ $(MAKE) OPT=-O0 PATH_PREFIX=$(DBGDIR)

$(DEPDIR)/%.d : %.c $(MAKEFILE_LIST)
	@echo "DEP		$@"
	@if test \( ! \( -d $(@D) \) \) ;then mkdir -p $(@D);fi
	$(VERBOSE) $(CC) $(CXXFLAGS) -MM -MT $(OBJDIR)/$*.o -MF $@ $<

$(OBJDIR)/%.o : %.c $(MAKEFILE_LIST)
	@echo "CC		$@"
	@if test \( ! \( -d $(@D) \) \) ;then mkdir -p $(@D);fi
	$(VERBOSE) $(CC) -c $(CCFLAGS) -o $@ $<

$(BINARY) : $(OBJPRE) $(MAKEFILE_LIST)
	@echo "CCLD		$@"
	@if test \( ! \( -d $(@D) \) \) ;then mkdir -p $(@D);fi
	$(VERBOSE) $(CC) $(CCFLAGS) -o $@ $(OBJPRE)

clean:
	@echo "MAKE clean	./runtime_library/"
	$(VERBOSE) $(MAKE) -C runtime_library/ clean
	@echo "RM		$(BINARY)"
	$(VERBOSE) rm -f $(BINARY)
	@echo "RM		$(OBJDIR)"
	$(VERBOSE) rm -rf $(OBJDIR)
	@echo "RM		$(DEPDIR)"
	$(VERBOSE) rm -rf $(DEPDIR)
	@echo "RM		$(DBGDIR)"
	$(VERBOSE) rm -rf $(DBGDIR)

ifneq ($(MAKECMDGOALS),clean)
-include $(DEP_FILES)
endif
