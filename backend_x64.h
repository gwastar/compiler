#ifndef __BACKEND_X64_INCLUDE__
#define __BACKEND_X64_INCLUDE__

#include <stdio.h>
#include "ir.h"

void generate_code(struct ir_program *program, const char *output_file_name);
void run_assembler(const char *input_file_name, const char *output_file_name);
void run_linker(const char **input_file_names, const char *output_file_name);

#define get_file_extension_assembler_code() "s"
#define get_file_extension_object_file() "o"

#endif
