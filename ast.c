#include <assert.h>
#include <stdio.h>
#include <inttypes.h>
#include "array.h"
#include "ast.h"
#include "compiler.h"
#include "error.h"

struct ast_node *__make_ast_node(enum ast_node_class class, size_t size, loc_t loc)
{
	static size_t next_serial = 0;

	struct ast_node *node = allocate_memory(size);
	node->serial = next_serial++;
	node->class = class;
	node->loc = loc;
	return node;
}

static const char *binop_to_string(enum binary_operator binop)
{
	switch (binop) {
	case BINOP_PLUS: return "+";
	case BINOP_MINUS: return "-";
	case BINOP_MUL: return "*";
	case BINOP_DIV: return "/";
	case BINOP_OR: return "or";
	case BINOP_AND: return "and";
	case BINOP_EQUAL: return "==";
	case BINOP_UNEQUAL: return "!=";
	case BINOP_LESSTHAN: return "<";
	case BINOP_LESSEQUAL: return "<=";
	case BINOP_GREATERTHAN: return ">";
	case BINOP_GREATEREQUAL: return ">=";
	case BINOP_INVALID:
	default:
		assert(false);
		return "INVALID";
	}
}

static void print_type_helper(type_t *type)
{
	if (!type) {
		printf("???");
		return;
	}

	if (type->class == TYPE_PRIMITIVE) {
		if (type == type_int) {
			printf("int");
		} else if (type == type_real) {
			printf("real");
		} else if (type == type_void) {
			printf("void");
		} else {
			assert(false);
		}
	} else if (type->class == TYPE_ARRAY) {
		print_type_helper(type->base_type);
		array_fori(type->lengths, i) {
			printf("[%zu]", type->lengths[i]);
		}
	} else {
		assert(type->class == TYPE_FUNCTION);
		printf("(");
		array_fori(type->parameter_types, i) {
			if (i != 0) {
				printf(", ");
			}
			print_type_helper(type->parameter_types[i]);
		}
		printf(") : ");
		print_type_helper(type->return_type);
	}
}

static void print_type(type_t *type)
{
	printf("\033[36m"); // cyan
	printf(" {");
	print_type_helper(type);
	printf("}");
	printf("\033[0m");
}

static void print_class(const char *class)
{
	printf("\033[33m"); // yellow
	printf("%s", class);
	printf("\033[0m");
}

static void print_symbol(symbol_t *symbol)
{
	printf("\033[32m"); // green
	printf(" (Symbol: %p)", (void *)symbol);
	printf("\033[0m");
}

static void dump_ast_node(struct ast_node *node, size_t indent)
{
	if (!node) {
		return;
	}

	for (size_t i = 0; i < indent; i++) {
		fputs("  ", stdout);
	}

	switch (node->class) {
	case AST_PROGRAM: {
		struct program *program = PROGRAM(node);
		print_class("Program");
		printf("\n");
		array_fori(program->declarations, i) {
			dump_ast_node(NODE(program->declarations[i]), indent + 1);
		}
		break;
	}
	case AST_BLOCK: {
		struct block *block = BLOCK(node);
		print_class("Block");
		printf("\n");
		array_fori(block->variable_declarations, i) {
			dump_ast_node(NODE(block->variable_declarations[i]), indent + 1);
		}
		array_fori(block->statements, i) {
			dump_ast_node(NODE(block->statements[i]), indent + 1);
		}
		break;
	}
	case AST_ARRAY_ACCESS: {
		struct array_access *array_access = ARRAY_ACCESS(node);
		print_class("Array Access");
		print_type(GET_TYPE(array_access));
		printf("\n");
		dump_ast_node(NODE(array_access->array_name), indent + 1);
		array_fori(array_access->indices, i) {
			dump_ast_node(NODE(array_access->indices[i]), indent + 1);
		}
		break;
	}
	case AST_BINARY_EXPRESSION: {
		struct binary_expression *binary_expression = BINARY_EXPRESSION(node);
		print_class("Binary Expression '%s'");
		printf(" '%s'", binop_to_string(binary_expression->operator));
		print_type(GET_TYPE(binary_expression));
		printf("\n");
		dump_ast_node(NODE(binary_expression->left_operand), indent + 1);
		dump_ast_node(NODE(binary_expression->right_operand), indent + 1);
		break;
	}
	case AST_CONSTANT: {
		struct constant *constant = CONSTANT(node);
		print_class("Constant");
		if (constant->constant_type == CONSTANT_U64) {
			printf(" '%" PRIu64 "'", constant->u64);
		} else {
			assert(constant->constant_type == CONSTANT_F64);
			printf(" '%f'", constant->f64);
		}
		print_type(GET_TYPE(constant));
		printf("\n");
		break;
	}
	case AST_IDENTIFIER: {
		struct identifier *identifier = IDENTIFIER(node);
		print_class("Identifier");
		printf(" '%s'", identifier->name->string);
		print_symbol(GET_SYMBOL(identifier));
		print_type(GET_TYPE(identifier));
		printf("\n");
		break;
	}
	case AST_FUNCTION_CALL: {
		struct function_call *function_call = FUNCTION_CALL(node);
		print_class("Function Call");
		print_type(GET_TYPE(function_call));
		printf("\n");
		dump_ast_node(NODE(function_call->function_name), indent + 1);
		array_fori(function_call->arguments, i) {
			dump_ast_node(NODE(function_call->arguments[i]), indent + 1);
		}
		break;
	}
	case AST_TYPE_CAST: {
		struct type_cast *type_cast = TYPE_CAST(node);
		print_class("Type Cast");
		print_type(GET_TYPE(type_cast));
		printf("\n");
		dump_ast_node(NODE(type_cast->casted_expression), indent + 1);
		dump_ast_node(NODE(type_cast->target_type_name), indent + 1);
		break;
	}
	case AST_ASSIGN_STATEMENT: {
		struct assign_statement *assign_statement = ASSIGN_STATEMENT(node);
		print_class("Assign Statement");
		printf("\n");
		dump_ast_node(NODE(assign_statement->lhs), indent + 1);
		dump_ast_node(NODE(assign_statement->rhs), indent + 1);
		break;
	}
	case AST_FUNCTION_CALL_STATEMENT: {
		struct function_call_statement *function_call_statement = FUNCTION_CALL_STATEMENT(node);
		print_class("Function Call Statement");
		printf("\n");
		dump_ast_node(NODE(function_call_statement->function_call), indent + 1);
		break;
	}
	case AST_IF_STATEMENT: {
		struct if_statement *if_statement = IF_STATEMENT(node);
		print_class("If Statement");
		printf("\n");
		dump_ast_node(NODE(if_statement->condition), indent + 1);
		dump_ast_node(NODE(if_statement->then_block), indent + 1);
		dump_ast_node(NODE(if_statement->else_block), indent + 1);
		break;
	}
	case AST_RETURN_STATEMENT: {
		struct return_statement *return_statement = RETURN_STATEMENT(node);
		print_class("Return Statement");
		printf("\n");
		dump_ast_node(NODE(return_statement->return_value), indent + 1);
		break;
	}
	case AST_WHILE_STATEMENT: {
		struct while_statement *while_statement = WHILE_STATEMENT(node);
		print_class("While Statement");
		printf("\n");
		dump_ast_node(NODE(while_statement->condition), indent + 1);
		dump_ast_node(NODE(while_statement->loop_body), indent + 1);
		break;
	}
	case AST_ARRAY_TYPE_NAME: {
		struct array_type_name *array_type_name = ARRAY_TYPE_NAME(node);
		print_class("Array Type Name");
		print_type(GET_TYPE(array_type_name));
		printf("\n");
		dump_ast_node(NODE(array_type_name->base_type_name), indent + 1);
		array_fori(array_type_name->size_expressions, i) {
			dump_ast_node(NODE(array_type_name->size_expressions[i]), indent + 1);
		}
		break;
	}
	case AST_PRIMITIVE_TYPE_NAME: {
		struct primitive_type_name *primitive_type_name = PRIMITIVE_TYPE_NAME(node);
		print_class("Primitive Type Name");
		if (primitive_type_name->primitive_type == PRIMITIVE_TYPE_INT) {
			printf(" 'int'");
		} else {
			assert(primitive_type_name->primitive_type == PRIMITIVE_TYPE_REAL);
			printf(" 'real'");
		}
		print_type(GET_TYPE(primitive_type_name));
		printf("\n");
		break;
	}
	case AST_FUNCTION_DECLARATION: {
		struct function_declaration *function_declaration = FUNCTION_DECLARATION(node);
		print_class("Function Declaration");
		print_symbol(GET_SYMBOL(function_declaration));
		print_type(GET_SYMBOL(function_declaration) ? GET_TYPE(function_declaration) : NULL);
		printf("\n");
		array_fori(function_declaration->parameter_declarations, i) {
			dump_ast_node(NODE(function_declaration->parameter_declarations[i]), indent + 1);
		}
		dump_ast_node(NODE(function_declaration->return_type_name), indent + 1);
		dump_ast_node(NODE(function_declaration->function_body), indent + 1);
		break;
	}
	case AST_PARAMETER_DECLARATION: {
		struct parameter_declaration *parameter_declaration = PARAMETER_DECLARATION(node);
		print_class("Parameter Declaration");
		print_symbol(GET_SYMBOL(parameter_declaration));
		print_type(GET_SYMBOL(parameter_declaration) ? GET_TYPE(parameter_declaration) : NULL);
		printf("\n");
		dump_ast_node(NODE(parameter_declaration->type_name), indent + 1);
		break;
	}
	case AST_VARIABLE_DECLARATION: {
		struct variable_declaration *variable_declaration = VARIABLE_DECLARATION(node);
		print_class("Variable Declaration");
		print_symbol(GET_SYMBOL(variable_declaration));
		print_type(GET_SYMBOL(variable_declaration) ? GET_TYPE(variable_declaration) : NULL);
		printf("\n");
		dump_ast_node(NODE(variable_declaration->type_name), indent + 1);
		dump_ast_node(NODE(variable_declaration->initializer), indent + 1);
		break;
	}
	case AST_INVALID:
	default:
		assert(false);
	}
}

void dump_ast(struct ast_node *ast)
{
	dump_ast_node(ast, 0);
}

/*
	switch (node->class) {
	case AST_PROGRAM: {
		struct program *program = PROGRAM(node);
		array_fori(program->declarations, i) {
			helper(NODE(program->declarations[i]), ctx);
		}
		break;
	}
	case AST_BLOCK: {
		struct block *block = BLOCK(node);
		array_fori(block->variable_declarations, i) {
			helper(NODE(block->variable_declarations[i]), ctx);
		}
		array_fori(block->statements, i) {
			helper(NODE(block->statements[i]), ctx);
		}
		break;
	}
	case AST_ARRAY_ACCESS: {
		struct array_access *array_access = ARRAY_ACCESS(node);
		helper(NODE(array_access->array_name), ctx);
		array_fori(array_access->indices, i) {
			helper(NODE(array_access->indices[i]), ctx);
		}
		break;
	}
	case AST_BINARY_EXPRESSION: {
		struct binary_expression *binary_expression = BINARY_EXPRESSION(node);
		helper(NODE(binary_expression->left_operand), ctx);
		helper(NODE(binary_expression->right_operand), ctx);
		break;
	}
	case AST_CONSTANT: {
		struct constant *constant = CONSTANT(node);
		break;
	}
	case AST_IDENTIFIER: {
		struct identifier *identifier = IDENTIFIER(node);
		break;
	}
	case AST_FUNCTION_CALL: {
		struct function_call *function_call = FUNCTION_CALL(node);
		helper(NODE(function_call->function_name), ctx);
		array_fori(function_call->arguments, i) {
			helper(NODE(function_call->arguments[i]), ctx);
		}
		break;
	}
	case AST_TYPE_CAST: {
		struct type_cast *type_cast = TYPE_CAST(node);
		helper(NODE(type_cast->casted_expression), ctx);
		helper(NODE(type_cast->target_type_name), ctx);
		break;
	}
	case AST_ASSIGN_STATEMENT: {
		struct assign_statement *assign_statement = ASSIGN_STATEMENT(node);
		helper(NODE(assign_statement->lhs), ctx);
		helper(NODE(assign_statement->rhs), ctx);
		break;
	}
	case AST_FUNCTION_CALL_STATEMENT: {
		struct function_call_statement *function_call_statement = FUNCTION_CALL_STATEMENT(node);
		helper(NODE(function_call_statement->function_call), ctx);
		break;
	}
	case AST_IF_STATEMENT: {
		struct if_statement *if_statement = IF_STATEMENT(node);
		helper(NODE(if_statement->condition), ctx);
		helper(NODE(if_statement->then_block), ctx);
		helper(NODE(if_statement->else_block), ctx);
		break;
	}
	case AST_RETURN_STATEMENT: {
		struct return_statement *return_statement = RETURN_STATEMENT(node);
		helper(NODE(return_statement->return_value), ctx);
		break;
	}
	case AST_WHILE_STATEMENT: {
		struct while_statement *while_statement = WHILE_STATEMENT(node);
		helper(NODE(while_statement->condition), ctx);
		helper(NODE(while_statement->loop_body), ctx);
		break;
	}
	case AST_ARRAY_TYPE_NAME: {
		struct array_type_name *array_type_name = ARRAY_TYPE_NAME(node);
		helper(NODE(array_type_name->base_type_name), ctx);
		array_fori(array_type_name->size_expressions, i) {
			helper(NODE(array_type_name->size_expressions[i]), ctx);
		}
		break;
	}
	case AST_PRIMITIVE_TYPE_NAME: {
		struct primitive_type_name *primitive_type_name = PRIMITIVE_TYPE_NAME(node);
		break;
	}
	case AST_FUNCTION_DECLARATION: {
		struct function_declaration *function_declaration = FUNCTION_DECLARATION(node);
		helper(NODE(function_declaration->declaration.ident), ctx);
		array_fori(function_declaration->parameter_declarations, i) {
			helper(NODE(function_declaration->parameter_declarations[i]), ctx);
		}
		helper(NODE(function_declaration->return_type_name), ctx);
		helper(NODE(function_declaration->function_body), ctx);
		break;
	}
	case AST_PARAMETER_DECLARATION: {
		struct parameter_declaration *parameter_declaration = PARAMETER_DECLARATION(node);
		helper(NODE(parameter_declaration->declaration.ident), ctx);
		helper(NODE(parameter_declaration->type_name), ctx);
		break;
	}
	case AST_VARIABLE_DECLARATION: {
		struct variable_declaration *variable_declaration = VARIABLE_DECLARATION(node);
		helper(NODE(variable_declaration->type_name), ctx);
		helper(NODE(variable_declaration->initializer), ctx);
		break;
	}
	case AST_INVALID:
	default:
		assert(false);
	}
*/
