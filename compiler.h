#ifndef __COMPILER_INCLUDE__
#define __COMPILER_INCLUDE__

#include <stdlib.h>
#include <stdio.h>
#include "ast.h"
#include "loc.h"

#define report_error(ASTNODE, SHOW_CONTEXT, FMT, ...) _report_error(NODE(ASTNODE)->loc, SHOW_CONTEXT, FMT,##__VA_ARGS__);

void __attribute__ ((format (printf, 3, 4), noreturn))
_report_error(loc_t loc, bool show_context, const char *fmt, ...);
void *allocate_memory(size_t size);

#endif
