#ifndef __TYPER_INCLUDE__
#define __TYPER_INCLUDE__

typedef struct type type_t;

#include "ast.h"
#include "atom.h"

struct type {
	enum type_class {
		TYPE_INVALID,
		TYPE_PRIMITIVE,
		TYPE_ARRAY,
		TYPE_FUNCTION,
	} class;
	union {
		atom_t *name; // primitive type
		struct { // array type
			type_t *base_type;
			size_t *lengths;
		};
		struct { // function type
			type_t *return_type;
			type_t **parameter_types;
		};
	};
};

extern type_t *type_int;
extern type_t *type_real;
extern type_t *type_void;

struct program;
void perform_type_checking(struct program *program);
type_t *make_type(enum type_class class);
void init_primitive_types(void);

#endif
