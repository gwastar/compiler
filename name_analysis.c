#include <stdlib.h>
#include <stdio.h>
#include "array.h"
#include "compiler.h"
#include "name_analysis.h"
#include "symbol.h"

struct context {
	struct symbol_table symbol_table;
	struct identifier **unresolved_identifiers;
	size_t program_scope;
};

static inline symbol_t *resolve(atom_t *name, struct context *ctx)
{
	return lookup_symbol(&ctx->symbol_table, name);
}

static inline symbol_t *resolve_identifier(struct identifier *ident, struct context *ctx)
{
	return resolve(ident->name, ctx);
}

static void declare_symbol(struct declaration *decl, enum symbol_class symbol_class, struct context *ctx)
{
	symbol_t *symbol = make_symbol(decl->name, decl, symbol_class);
	if (!put_symbol(&ctx->symbol_table, symbol)) {
		report_error(decl, true,  "redeclaration of symbol '%s'", decl->name->string);
	}
	SET_SYMBOL(decl, symbol);
}

static void helper(struct ast_node *node, struct context *ctx)
{
	if (!node) {
		return;
	}

	switch (node->class) {
	case AST_PROGRAM: {
		struct program *program = PROGRAM(node);

		enter_scope(&ctx->symbol_table);
		ctx->program_scope = array_lasti(ctx->symbol_table.scope_stack); // @Cleanup

		array_fori(program->declarations, i) {
			helper(NODE(program->declarations[i]), ctx);
		}

		// @Cleanup make array_foreach_value
		struct identifier **ident;
		array_foreach(ctx->unresolved_identifiers, ident) {
			symbol_t *symbol = resolve_identifier(*ident, ctx);
			if (!symbol) {
				report_error(*ident, true,  "unresolved identifier '%s'",
					     (*ident)->name->string);
			}
			assert(symbol->class == SYMBOL_FUNCTION ||
			       symbol->class == SYMBOL_GLOBAL_VARIABLE);
			SET_SYMBOL(*ident, symbol);
		}

		leave_scope(&ctx->symbol_table);

		break;
	}
	case AST_BLOCK: {
		struct block *block = BLOCK(node);

		enter_scope(&ctx->symbol_table);

		array_fori(block->variable_declarations, i) {
			helper(NODE(block->variable_declarations[i]), ctx);
		}
		array_fori(block->statements, i) {
			helper(NODE(block->statements[i]), ctx);
		}

		leave_scope(&ctx->symbol_table);

		break;
	}
	case AST_ARRAY_ACCESS: {
		struct array_access *array_access = ARRAY_ACCESS(node);
		helper(NODE(array_access->array_name), ctx);
		array_fori(array_access->indices, i) {
			helper(NODE(array_access->indices[i]), ctx);
		}
		break;
	}
	case AST_BINARY_EXPRESSION: {
		struct binary_expression *binary_expression = BINARY_EXPRESSION(node);
		helper(NODE(binary_expression->left_operand), ctx);
		helper(NODE(binary_expression->right_operand), ctx);
		break;
	}
	case AST_CONSTANT: {
		struct constant *constant = CONSTANT(node);
		break;
	}
	case AST_IDENTIFIER: {
		struct identifier *identifier = IDENTIFIER(node);

		symbol_t *symbol = resolve_identifier(identifier, ctx);
		if (symbol) {
			SET_SYMBOL(identifier, symbol);
		} else {
			array_add(ctx->unresolved_identifiers, identifier);
		}

		break;
	}
	case AST_FUNCTION_CALL: {
		struct function_call *function_call = FUNCTION_CALL(node);
		helper(NODE(function_call->function_name), ctx);
		array_fori(function_call->arguments, i) {
			helper(NODE(function_call->arguments[i]), ctx);
		}
		break;
	}
	case AST_TYPE_CAST: {
		struct type_cast *type_cast = TYPE_CAST(node);
		helper(NODE(type_cast->casted_expression), ctx);
		helper(NODE(type_cast->target_type_name), ctx);
		break;
	}
	case AST_ASSIGN_STATEMENT: {
		struct assign_statement *assign_statement = ASSIGN_STATEMENT(node);
		helper(NODE(assign_statement->lhs), ctx);
		helper(NODE(assign_statement->rhs), ctx);
		break;
	}
	case AST_FUNCTION_CALL_STATEMENT: {
		struct function_call_statement *function_call_statement = FUNCTION_CALL_STATEMENT(node);
		helper(NODE(function_call_statement->function_call), ctx);
		break;
	}
	case AST_IF_STATEMENT: {
		struct if_statement *if_statement = IF_STATEMENT(node);
		helper(NODE(if_statement->condition), ctx);
		helper(NODE(if_statement->then_block), ctx);
		helper(NODE(if_statement->else_block), ctx);
		break;
	}
	case AST_RETURN_STATEMENT: {
		struct return_statement *return_statement = RETURN_STATEMENT(node);
		helper(NODE(return_statement->return_value), ctx);
		break;
	}
	case AST_WHILE_STATEMENT: {
		struct while_statement *while_statement = WHILE_STATEMENT(node);
		helper(NODE(while_statement->condition), ctx);
		helper(NODE(while_statement->loop_body), ctx);
		break;
	}
	case AST_ARRAY_TYPE_NAME: {
		struct array_type_name *array_type_name = ARRAY_TYPE_NAME(node);
		helper(NODE(array_type_name->base_type_name), ctx);
		array_fori(array_type_name->size_expressions, i) {
			helper(NODE(array_type_name->size_expressions[i]), ctx);
		}
		break;
	}
	case AST_PRIMITIVE_TYPE_NAME: {
		struct primitive_type_name *primitive_type_name = PRIMITIVE_TYPE_NAME(node);
		break;
	}
	case AST_FUNCTION_DECLARATION: {
		struct function_declaration *function_declaration = FUNCTION_DECLARATION(node);

		declare_symbol(DECLARATION(function_declaration), SYMBOL_FUNCTION, ctx);

		enter_scope(&ctx->symbol_table);

		array_fori(function_declaration->parameter_declarations, i) {
			helper(NODE(function_declaration->parameter_declarations[i]), ctx);
		}
		helper(NODE(function_declaration->return_type_name), ctx);
		helper(NODE(function_declaration->function_body), ctx);

		leave_scope(&ctx->symbol_table);

		break;
	}
	case AST_PARAMETER_DECLARATION: {
		struct parameter_declaration *parameter_declaration = PARAMETER_DECLARATION(node);

		declare_symbol(&parameter_declaration->declaration, SYMBOL_PARAMETER, ctx);

		helper(NODE(parameter_declaration->type_name), ctx);
		break;
	}
	case AST_VARIABLE_DECLARATION: {
		struct variable_declaration *variable_declaration = VARIABLE_DECLARATION(node);

		enum symbol_class class = array_lasti(ctx->symbol_table.scope_stack) == ctx->program_scope ?
			SYMBOL_GLOBAL_VARIABLE : SYMBOL_LOCAL_VARIABLE;
		declare_symbol(&variable_declaration->declaration, class, ctx);

		helper(NODE(variable_declaration->type_name), ctx);
		helper(NODE(variable_declaration->initializer), ctx);
		break;
	}
	case AST_INVALID:
	default:
		assert(false);
	}
}

static void init_predefined_functions(struct symbol_table *table)
{
	// @Cleanup
#define ADD_PARAMS0()
#define ADD_PARAMS1(PARAMS)						\
	type_t *params[] = PARAMS;					\
	array_add_arrayn(type->parameter_types, params, sizeof(params) / sizeof(params[0]));
#define ADD_PARAMS_X(x, a, b, ...) b
#define ADD_PARAMS(...) ADD_PARAMS_X(,##__VA_ARGS__, ADD_PARAMS1, ADD_PARAMS0)(__VA_ARGS__)

#define PREDEFINED_FUNCTION(NAME, RETURN_TYPE, ...)			\
	{								\
		atom_t *name = make_atom(#NAME, sizeof(#NAME) - 1);	\
		symbol_t *symbol = make_symbol(name, NULL, SYMBOL_FUNCTION); \
		type_t *type = make_type(TYPE_FUNCTION);		\
		type->return_type = RETURN_TYPE;			\
		type->parameter_types = NULL;				\
		ADD_PARAMS(__VA_ARGS__);				\
		symbol->type = type;					\
		bool success = put_symbol(table, symbol);		\
		assert(success);					\
	}

	PREDEFINED_FUNCTION(readChar, type_int);
	PREDEFINED_FUNCTION(readInt, type_int);
	PREDEFINED_FUNCTION(readReal, type_int);
	PREDEFINED_FUNCTION(writeChar, type_int, {type_int});
	PREDEFINED_FUNCTION(writeInt, type_int, {type_int});
	PREDEFINED_FUNCTION(writeReal, type_int, {type_real});
	PREDEFINED_FUNCTION(exit, type_int, {type_int});
	PREDEFINED_FUNCTION(time, type_int);
}

void perform_name_analysis(struct program *program)
{
	struct context ctx = {
		.unresolved_identifiers = NULL,
	};
	symbol_table_init(&ctx.symbol_table);
	enter_scope(&ctx.symbol_table);
	init_predefined_functions(&ctx.symbol_table);
	helper(NODE(program), &ctx);
	leave_scope(&ctx.symbol_table);
	symbol_table_destroy(&ctx.symbol_table);
}
