#ifndef __SYMBOL_INCLUDE__
#define __SYMBOL_INCLUDE__

typedef struct symbol symbol_t;

#include <stdbool.h>
#include "ast.h"
#include "atom.h"
#include "hashtable.h"
#include "ir.h"
#include "typer.h"

struct symbol {
	atom_t *name;
	type_t *type;
	struct declaration *declaration;
	struct ir_variable *ir_variable;
	enum symbol_class {
		SYMBOL_INVALID,
		SYMBOL_FUNCTION,
		SYMBOL_GLOBAL_VARIABLE,
		SYMBOL_LOCAL_VARIABLE,
		SYMBOL_PARAMETER,
	} class;
};

struct symbol_table {
	struct scope_table *scope_stack;
};

symbol_t *make_symbol(atom_t *name, struct declaration *decl, enum symbol_class symbol_class);
void symbol_table_init(struct symbol_table *table);
void symbol_table_destroy(struct symbol_table *table);
void enter_scope(struct symbol_table *table);
void leave_scope(struct symbol_table *table);
bool put_symbol(struct symbol_table *table, symbol_t *symbol);
symbol_t *lookup_symbol(struct symbol_table *table, atom_t *name);

#endif
