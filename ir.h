#ifndef __IR_INCLUDE__
#define __IR_INCLUDE__

#include <inttypes.h>
#include <stdbool.h>
#include "typer.h"

struct ir_program {
	struct ir_variable **global_variables;
	struct ir_function **functions;
};

struct ir_function {
	atom_t *name;
	struct ir_instruction **instructions;
	struct ir_variable **parameters;
	struct ir_variable **local_variables;
	struct ir_variable **virtual_registers;
};

struct ir_operand {
	enum operand_class {
		OPERAND_INVALID,
		OPERAND_VARIABLE,
		OPERAND_CONSTANT,
	} class;
	type_t *type;
};

struct ir_variable {
	struct ir_operand operand;
	atom_t *name; // not unique for local variables!
	bool global;
	int64_t stack_offset;
};

struct ir_constant {
	struct ir_operand operand;
	union {
		uint64_t u64;
		double f64;
	};
};

struct ir_instruction {
	enum instruction_class {
		INSTRUCTION_INVALID,
		INSTRUCTION_NOP,
		INSTRUCTION_LABEL,
		INSTRUCTION_MOV,
		INSTRUCTION_LOAD,
		INSTRUCTION_STORE,
		INSTRUCTION_ADD,
		INSTRUCTION_SUB,
		INSTRUCTION_MUL,
		INSTRUCTION_DIV,
		INSTRUCTION_JMP,
		INSTRUCTION_JEQ,
		INSTRUCTION_JNE,
		INSTRUCTION_JLT,
		INSTRUCTION_JLE,
		INSTRUCTION_JGT,
		INSTRUCTION_JGE,
		INSTRUCTION_CALL,
		INSTRUCTION_RET,
		INSTRUCTION_I2R,
		INSTRUCTION_R2I,
	} class;
};

struct jump {
	struct ir_instruction instruction;
	struct label *target;
};

struct conditional_jump {
	struct jump jump;
	struct ir_operand *left_operand;
	struct ir_operand *right_operand;
};

struct assignment {
	struct ir_instruction instruction;
	struct ir_variable *target_variable;
};

struct binary_operation {
	struct assignment assignment;
	struct ir_operand *left_operand;
	struct ir_operand *right_operand;
};

struct cast_operation {
	struct assignment assignment;
	struct ir_operand *casted_operand;
};

struct nop {
	struct ir_instruction instruction;
};

struct label {
	struct ir_instruction instruction;
	size_t number;
};

struct mov {
	struct assignment assignment;
	struct ir_operand *source_operand;
};

struct load {
	struct assignment assignment;
	struct ir_variable *array_variable;
	struct ir_operand *index;
};

struct store {
	struct ir_instruction instruction;
	struct ir_variable *array_variable;
	struct ir_operand *index;
	struct ir_operand *source_operand;
};

struct add {
	struct binary_operation binary_operation;
};

struct sub {
	struct binary_operation binary_operation;
};

struct mul {
	struct binary_operation binary_operation;
};

struct div {
	struct binary_operation binary_operation;
};

struct jmp {
	struct jump jump;
};

struct jeq {
	struct conditional_jump conditional_jump;
};

struct jne {
	struct conditional_jump conditional_jump;
};

struct jlt {
	struct conditional_jump conditional_jump;
};

struct jle {
	struct conditional_jump conditional_jump;
};

struct jgt {
	struct conditional_jump conditional_jump;
};

struct jge {
	struct conditional_jump conditional_jump;
};

struct call {
	struct assignment assignment;
	atom_t *callee;
	struct ir_operand **arguments;
};

struct ret {
	struct ir_instruction instruction;
	struct ir_operand *return_value;
};

struct i2r {
	struct cast_operation cast_operation;
};

struct r2i {
	struct cast_operation cast_operation;
};

struct program;
struct ir_program *generate_ir(struct program *program);
void dump_ir(struct ir_program *program);

struct ir_instruction *__make_instruction(enum instruction_class class, size_t size);
#define __MAKE_INSTRUCTION(type_name, class) ((struct type_name *)__make_instruction(class, sizeof(struct type_name)))

#define MAKE_NOP()   __MAKE_INSTRUCTION(nop, INSTRUCTION_NOP)
#define MAKE_LABEL() __MAKE_INSTRUCTION(label, INSTRUCTION_LABEL)
#define MAKE_MOV()   __MAKE_INSTRUCTION(mov, INSTRUCTION_MOV)
#define MAKE_LOAD()  __MAKE_INSTRUCTION(load, INSTRUCTION_LOAD)
#define MAKE_STORE() __MAKE_INSTRUCTION(store, INSTRUCTION_STORE)
#define MAKE_ADD()   __MAKE_INSTRUCTION(add, INSTRUCTION_ADD)
#define MAKE_SUB()   __MAKE_INSTRUCTION(sub, INSTRUCTION_SUB)
#define MAKE_MUL()   __MAKE_INSTRUCTION(mul, INSTRUCTION_MUL)
#define MAKE_DIV()   __MAKE_INSTRUCTION(div, INSTRUCTION_DIV)
#define MAKE_JMP()   __MAKE_INSTRUCTION(jmp, INSTRUCTION_JMP)
#define MAKE_JEQ()   __MAKE_INSTRUCTION(jeq, INSTRUCTION_JEQ)
#define MAKE_JNE()   __MAKE_INSTRUCTION(jne, INSTRUCTION_JNE)
#define MAKE_JLT()   __MAKE_INSTRUCTION(jlt, INSTRUCTION_JLT)
#define MAKE_JLE()   __MAKE_INSTRUCTION(jle, INSTRUCTION_JLE)
#define MAKE_JGT()   __MAKE_INSTRUCTION(jgt, INSTRUCTION_JGT)
#define MAKE_JGE()   __MAKE_INSTRUCTION(jge, INSTRUCTION_JGE)
#define MAKE_CALL()  __MAKE_INSTRUCTION(call, INSTRUCTION_CALL)
#define MAKE_RET()   __MAKE_INSTRUCTION(ret, INSTRUCTION_RET)
#define MAKE_I2R()   __MAKE_INSTRUCTION(i2r, INSTRUCTION_I2R)
#define MAKE_R2I()   __MAKE_INSTRUCTION(r2i, INSTRUCTION_R2I)

#define NOP(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_NOP), ((struct nop *)(INSTR)))
#define LABEL(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_LABEL), ((struct label *)(INSTR)))
#define MOV(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_MOV), ((struct mov *)(INSTR)))
#define LOAD(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_LOAD), ((struct load *)(INSTR)))
#define STORE(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_STORE), ((struct store *)(INSTR)))
#define ADD(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_ADD), ((struct add *)(INSTR)))
#define SUB(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_SUB), ((struct sub *)(INSTR)))
#define MUL(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_MUL), ((struct mul *)(INSTR)))
#define DIV(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_DIV), ((struct div *)(INSTR)))
#define JMP(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_JMP), ((struct jmp *)(INSTR)))
#define JEQ(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_JEQ), ((struct jeq *)(INSTR)))
#define JNE(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_JNE), ((struct jne *)(INSTR)))
#define JLT(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_JLT), ((struct jlt *)(INSTR)))
#define JLE(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_JLE), ((struct jle *)(INSTR)))
#define JGT(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_JGT), ((struct jgt *)(INSTR)))
#define JGE(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_JGE), ((struct jge *)(INSTR)))
#define CALL(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_CALL), ((struct call *)(INSTR)))
#define RET(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_RET), ((struct ret *)(INSTR)))
#define I2R(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_I2R), ((struct i2r *)(INSTR)))
#define R2I(INSTR) (assert(INSTRUCTION(INSTR)->class == INSTRUCTION_R2I), ((struct r2i *)(INSTR)))

#define ASSIGNMENT(INSTR) ((struct assignment *)(INSTR))
#define CAST_OPERATION(INSTR) ((struct cast_operation *)(INSTR))
#define BINARY_OPERATION(INSTR) ((struct binary_operation *)(INSTR))
#define JUMP(INSTR) ((struct jump *)(INSTR))
#define CONDITIONAL_JUMP(INSTR) ((struct conditional_jump *)(INSTR))

#define INSTRUCTION(INSTR) _Generic(					\
		(INSTR),						\
		struct assignment *: &((struct assignment *)(INSTR))->instruction, \
		struct cast_operation *: &((struct cast_operation *)(INSTR))->assignment.instruction, \
		struct binary_operation *: &((struct binary_operation *)(INSTR))->assignment.instruction, \
		struct jump *: &((struct jump *)(INSTR))->instruction,	\
		struct conditional_jump *: &((struct conditional_jump *)(INSTR))->jump.instruction, \
		struct nop *: &((struct nop *)(INSTR))->instruction,	\
		struct label *: &((struct label *)(INSTR))->instruction, \
		struct mov *: &((struct mov *)(INSTR))->assignment.instruction, \
		struct load *: &((struct load *)(INSTR))->assignment.instruction, \
		struct store *: &((struct store *)(INSTR))->instruction, \
		struct add *: &((struct add *)(INSTR))->binary_operation.assignment.instruction, \
		struct sub *: &((struct sub *)(INSTR))->binary_operation.assignment.instruction, \
		struct mul *: &((struct mul *)(INSTR))->binary_operation.assignment.instruction, \
		struct div *: &((struct div *)(INSTR))->binary_operation.assignment.instruction, \
		struct jmp *: &((struct jmp *)(INSTR))->jump.instruction, \
		struct jeq *: &((struct jeq *)(INSTR))->conditional_jump.jump.instruction, \
		struct jne *: &((struct jne *)(INSTR))->conditional_jump.jump.instruction, \
		struct jlt *: &((struct jlt *)(INSTR))->conditional_jump.jump.instruction, \
		struct jle *: &((struct jle *)(INSTR))->conditional_jump.jump.instruction, \
		struct jgt *: &((struct jgt *)(INSTR))->conditional_jump.jump.instruction, \
		struct jge *: &((struct jge *)(INSTR))->conditional_jump.jump.instruction, \
		struct call *: &((struct call *)(INSTR))->assignment.instruction, \
		struct ret *: &((struct ret *)(INSTR))->instruction,	\
		struct i2r *: &((struct i2r *)(INSTR))->cast_operation.assignment.instruction, \
		struct r2i *: &((struct r2i *)(INSTR))->cast_operation.assignment.instruction, \
		struct ir_instruction *: (INSTR))

#define IS_IR_CONSTANT(OP) _Generic((OP),				\
				   struct ir_variable *: false,		\
				   struct ir_constant *: true,		\
				   struct ir_operand *: (OP)->class == OPERAND_CONSTANT)

#define IS_IR_VARIABLE(OP) _Generic((OP),				\
				   struct ir_variable *: true,		\
				   struct ir_constant *: false,		\
				   struct ir_operand *: (OP)->class == OPERAND_VARIABLE)

#define IR_VARIABLE(OP) (assert(IS_IR_VARIABLE(OP)), (struct ir_variable *)(OP))
#define IR_CONSTANT(OP) (assert(IS_IR_CONSTANT(OP)), (struct ir_constant *)(OP))
#define IR_OPERAND(OP) _Generic((OP),					\
				struct ir_variable *: &((struct ir_variable *)(OP))->operand, \
				struct ir_constant *: &((struct ir_constant *)(OP))->operand, \
				struct ir_operand *: (OP))


#endif
