#ifndef __PARSER_INCLUDE__
#define __PARSER_INCLUDE__

#include "ast.h"
#include "tokenizer.h"

struct program *parse(token_t *tokens);

#endif
