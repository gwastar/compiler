#ifndef __CONSTANT_FOLDING_INCLUDE__
#define __CONSTANT_FOLDING_INCLUDE__

#include "ast.h"

void perform_constant_folding(struct program *program);

#endif
