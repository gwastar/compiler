#include <assert.h>
#include <stdio.h>
#include "array.h"
#include "ast.h"
#include "atom.h"
#include "compiler.h"
#include "ir.h"
#include "mprintf.h"

struct context {
	struct ir_program *program;
	struct ir_function *surrounding_function;
	size_t label_counter;
	size_t virtual_register_counter;
};

struct ir_instruction *__make_instruction(enum instruction_class class, size_t size)
{
	struct ir_instruction *instr = allocate_memory(size);
	instr->class = class;
	return instr;
}

struct ir_variable *make_ir_variable(atom_t *name, type_t *type, bool global)
{
	struct ir_variable *var = allocate_memory(sizeof(*var));
	var->operand.class = OPERAND_VARIABLE;
	var->operand.type = type;
	var->name = name;
	var->global = global;
	return var;
}

struct ir_constant *make_int_constant(uint64_t value)
{
	struct ir_constant *constant = allocate_memory(sizeof(*constant));
	constant->operand.class = OPERAND_CONSTANT;
	constant->operand.type = type_int;
	constant->u64 = value;
	return constant;
}

struct ir_constant *make_real_constant(double value)
{
	struct ir_constant *constant = allocate_memory(sizeof(*constant));
	constant->operand.class = OPERAND_CONSTANT;
	constant->operand.type = type_real;
	constant->f64 = value;
	return constant;
}

struct ir_constant *make_default_constant(type_t *type)
{
	if (type == type_void) {
		return NULL;
	} else if (type == type_int) {
		return make_int_constant(0);
	} else {
		assert(type == type_real);
		return make_real_constant(0);
	}
}

struct ir_variable *create_virtual_register(type_t *type, struct context *ctx)
{
	struct ir_function *function = ctx->surrounding_function;
	char *string = mprintf("%%v%zu", ctx->virtual_register_counter);
	atom_t *name = make_atom(string, strlen(string));
	struct ir_variable *var = make_ir_variable(name, type, false);
	array_add(function->virtual_registers, var);
	ctx->virtual_register_counter++;
	return var;
}

struct label *create_label(struct context *ctx)
{
	struct label *label = MAKE_LABEL();
	label->number = ctx->label_counter++;
	return label;
}

#define add_instruction(instr, ctx) _add_instruction(INSTRUCTION(instr), ctx)
static void _add_instruction(struct ir_instruction *instr, struct context *ctx)
{
	struct ir_function *function = ctx->surrounding_function;
	assert(function);
	array_add(function->instructions, instr);
}

static struct ir_operand *helper(struct ast_node *node, struct context *ctx);

static void emit_code_for_condition(struct expression *condition, struct label *true_label,
				    struct label *false_label, struct context *ctx)
{
	struct binary_expression *binexpr = BINARY_EXPRESSION(condition);
	struct expression *lhs = binexpr->left_operand;
	struct expression *rhs = binexpr->right_operand;

	if (binexpr->operator == BINOP_AND) {
		struct label *label = create_label(ctx);
		emit_code_for_condition(lhs, label, false_label, ctx);
		add_instruction(label, ctx);
		emit_code_for_condition(rhs, true_label, false_label, ctx);
		return;
	} else if (binexpr->operator == BINOP_OR) {
		struct label *label = create_label(ctx);
		emit_code_for_condition(lhs, true_label, label, ctx);
		add_instruction(label, ctx);
		emit_code_for_condition(rhs, true_label, false_label, ctx);
		return;
	}

	struct ir_operand *left = helper(NODE(lhs), ctx);
	struct ir_operand *right = helper(NODE(rhs), ctx);
	struct conditional_jump *cjmp = NULL;
	switch (binexpr->operator) {
	case BINOP_EQUAL:        cjmp = CONDITIONAL_JUMP(MAKE_JEQ()); break;
	case BINOP_UNEQUAL:      cjmp = CONDITIONAL_JUMP(MAKE_JNE()); break;
	case BINOP_LESSTHAN:     cjmp = CONDITIONAL_JUMP(MAKE_JLT()); break;
	case BINOP_LESSEQUAL:    cjmp = CONDITIONAL_JUMP(MAKE_JLE()); break;
	case BINOP_GREATERTHAN:  cjmp = CONDITIONAL_JUMP(MAKE_JGT()); break;
	case BINOP_GREATEREQUAL: cjmp = CONDITIONAL_JUMP(MAKE_JGE()); break;
	default: assert(false); break;
	}
	cjmp->jump.target = true_label;
	cjmp->left_operand = left;
	cjmp->right_operand = right;
	add_instruction(cjmp, ctx);

	struct jmp *jmp = MAKE_JMP();
	jmp->jump.target = false_label;
	add_instruction(jmp, ctx);
}

static struct ir_operand *compute_index(struct array_access *array_access, struct context *ctx)
{
	type_t *type = GET_TYPE(array_access->array_name);
	assert(type->class == TYPE_ARRAY);

	if (array_len(array_access->indices) == 1) {
		return helper(NODE(array_access->indices[0]), ctx);
	}

	struct ir_operand **indices = NULL;
	array_fori(array_access->indices, i) {
		struct ir_operand *idx = helper(NODE(array_access->indices[i]), ctx);
		array_add(indices, idx);
	}
	assert(!array_empty(indices));

	struct ir_constant **factors = NULL;
	uint64_t f = 1;
	array_fori(type->lengths, i) {
		array_add(factors, make_int_constant(f));
		f *= type->lengths[i];
	}

	struct ir_variable *var = create_virtual_register(type_int, ctx);
	struct mov *mov = MAKE_MOV();
	mov->assignment.target_variable = var;
	mov->source_operand = IR_OPERAND(make_int_constant(0));
	add_instruction(mov, ctx);

	array_fori(indices, i) {
		struct ir_operand *idx = indices[i];
		struct ir_constant *factor = factors[array_lasti(indices) - i];

		struct ir_variable *tmp = create_virtual_register(type_int, ctx);
		struct mul *mul = MAKE_MUL();
		mul->binary_operation.assignment.target_variable = tmp;
		mul->binary_operation.left_operand = idx;
		mul->binary_operation.right_operand = &factor->operand;
		add_instruction(mul, ctx);

		struct add *add = MAKE_ADD();
		add->binary_operation.assignment.target_variable = var;
		add->binary_operation.left_operand = &var->operand;
		add->binary_operation.right_operand = &tmp->operand;
		add_instruction(add, ctx);
	}

	array_free(indices);
	array_free(factors);
	return IR_OPERAND(var);
}

static struct ir_operand *helper(struct ast_node *node, struct context *ctx)
{
	if (!node) {
		return NULL;
	}

	struct ir_operand *retval = NULL;

	switch (node->class) {
	case AST_PROGRAM: {
		struct program *program = PROGRAM(node);

		assert(!ctx->program);
		ctx->program = allocate_memory(sizeof(*ctx->program));

		array_fori(program->declarations, i) {
			struct declaration *decl = program->declarations[i];
			if (NODE(decl)->class != AST_VARIABLE_DECLARATION) {
				continue;
			}
			// @Cleanup just handle this under case AST_VARIABLE_DECLARATION?
			symbol_t *symbol = GET_SYMBOL(decl);
			assert(symbol->class == SYMBOL_GLOBAL_VARIABLE);
			assert(!VARIABLE_DECLARATION(decl)->initializer);
			struct ir_variable *var = make_ir_variable(symbol->name, symbol->type, true);
			symbol->ir_variable = var;
			array_add(ctx->program->global_variables, var);
		}

		array_fori(program->declarations, i) {
			helper(NODE(program->declarations[i]), ctx);
		}
		break;
	}
	case AST_BLOCK: {
		struct block *block = BLOCK(node);
		array_fori(block->variable_declarations, i) {
			helper(NODE(block->variable_declarations[i]), ctx);
		}
		array_fori(block->statements, i) {
			helper(NODE(block->statements[i]), ctx);
		}
		break;
	}
	case AST_ARRAY_ACCESS: {
		struct array_access *array_access = ARRAY_ACCESS(node);
		struct ir_variable *array = IR_VARIABLE(helper(NODE(array_access->array_name), ctx));
		struct ir_operand *index = compute_index(array_access, ctx);
		struct ir_variable *target = create_virtual_register(GET_TYPE(array_access), ctx);
		struct load *load = MAKE_LOAD();
		load->assignment.target_variable = target;
		load->array_variable = array;
		load->index = index;
		add_instruction(load, ctx);
		retval = IR_OPERAND(target);
		break;
	}
	case AST_BINARY_EXPRESSION: {
		struct binary_expression *binary_expression = BINARY_EXPRESSION(node);
		struct ir_operand *operand1 = helper(NODE(binary_expression->left_operand), ctx);
		struct ir_operand *operand2 = helper(NODE(binary_expression->right_operand), ctx);
		struct ir_variable *target = create_virtual_register(GET_TYPE(binary_expression), ctx);

		struct binary_operation *binop = NULL;
		switch (binary_expression->operator) {
		case BINOP_PLUS:  binop = BINARY_OPERATION(MAKE_ADD()); break;
		case BINOP_MINUS: binop = BINARY_OPERATION(MAKE_SUB()); break;
		case BINOP_MUL:   binop = BINARY_OPERATION(MAKE_MUL()); break;
		case BINOP_DIV:   binop = BINARY_OPERATION(MAKE_DIV()); break;
		default: assert(false); break;
		}
		binop->assignment.target_variable = target;
		binop->left_operand = operand1;
		binop->right_operand = operand2;
		add_instruction(binop, ctx);
		retval = IR_OPERAND(target);
		break;
	}
	case AST_CONSTANT: {
		struct constant *constant = CONSTANT(node);
		type_t *type = GET_TYPE(constant);
		struct ir_constant *ir_constant;
		if (type == type_int) {
			ir_constant = make_int_constant(constant->u64);
		} else {
			assert(type == type_real);
			ir_constant = make_real_constant(constant->f64);
		}
		retval = IR_OPERAND(ir_constant);
		break;
	}
	case AST_IDENTIFIER: {
		struct identifier *identifier = IDENTIFIER(node);
		symbol_t *symbol = GET_SYMBOL(identifier);
		if (symbol->class == SYMBOL_GLOBAL_VARIABLE ||
		    symbol->class == SYMBOL_LOCAL_VARIABLE ||
		    symbol->class == SYMBOL_PARAMETER) {
			assert(symbol->ir_variable);
			retval = IR_OPERAND(symbol->ir_variable);
		}
		break;
	}
	case AST_FUNCTION_CALL: {
		struct function_call *function_call = FUNCTION_CALL(node);
		helper(NODE(function_call->function_name), ctx);
		struct ir_variable *target = NULL;
		type_t *type = GET_TYPE(function_call);
		if (type != type_void) {
			target = create_virtual_register(type, ctx);
		}

		struct ir_operand **arguments = NULL;
		array_fori(function_call->arguments, i) {
			struct ir_operand *arg = helper(NODE(function_call->arguments[i]), ctx);
			array_add(arguments, arg);
		}
		struct call *call = MAKE_CALL();
		call->assignment.target_variable = target;
		call->callee = function_call->function_name->name;
		call->arguments = arguments;
		add_instruction(call, ctx);
		retval = IR_OPERAND(target);
		break;
	}
	case AST_TYPE_CAST: {
		struct type_cast *type_cast = TYPE_CAST(node);
		struct ir_operand *operand = helper(NODE(type_cast->casted_expression), ctx);
		helper(NODE(type_cast->target_type_name), ctx);
		type_t *type = GET_TYPE(type_cast);
		struct ir_variable *target = create_virtual_register(type, ctx);
		struct cast_operation *cast;
		if (type == type_int) {
			assert(operand->type == type_real);
			cast = &MAKE_R2I()->cast_operation;
		} else {
			assert(type == type_real);
			assert(operand->type == type_int);
			cast = &MAKE_I2R()->cast_operation;
		}
		cast->assignment.target_variable = target;
		cast->casted_operand = operand;
		add_instruction(cast, ctx);
		retval = IR_OPERAND(target);
		break;
	}
	case AST_ASSIGN_STATEMENT: {
		struct assign_statement *assign_statement = ASSIGN_STATEMENT(node);
		struct expression *lhs = assign_statement->lhs;
		struct expression *rhs = assign_statement->rhs;

		struct ir_operand *source = helper(NODE(rhs), ctx);

		if (NODE(lhs)->class == AST_ARRAY_ACCESS) {
			struct array_access *array_access = ARRAY_ACCESS(lhs);
			struct ir_operand *index = compute_index(array_access, ctx);
			struct ir_variable *array_variable = GET_SYMBOL(array_access->array_name)->ir_variable;
			struct store *store = MAKE_STORE();
			store->array_variable = array_variable;
			store->index = index;
			store->source_operand = source;
			add_instruction(store, ctx);
			break;
		}

		struct identifier *ident = IDENTIFIER(lhs);
		struct ir_variable *var = GET_SYMBOL(ident)->ir_variable;
		struct mov *mov = MAKE_MOV();
		mov->assignment.target_variable = var;
		mov->source_operand = source;
		add_instruction(mov, ctx);
		break;
	}
	case AST_FUNCTION_CALL_STATEMENT: {
		struct function_call_statement *function_call_statement = FUNCTION_CALL_STATEMENT(node);
		helper(NODE(function_call_statement->function_call), ctx);
		break;
	}
	case AST_IF_STATEMENT: {
		struct if_statement *if_statement = IF_STATEMENT(node);

		struct label *true_label = create_label(ctx);
		struct label *false_label = create_label(ctx);

		emit_code_for_condition(if_statement->condition, true_label, false_label, ctx);

		add_instruction(true_label, ctx);
		helper(NODE(if_statement->then_block), ctx);

		if (if_statement->else_block) {
			struct label *end_label = create_label(ctx);
			struct jmp *jmp = MAKE_JMP();
			jmp->jump.target = end_label;
			add_instruction(jmp, ctx);
			add_instruction(false_label, ctx);
			helper(NODE(if_statement->else_block), ctx);
			add_instruction(end_label, ctx);
		} else {
			add_instruction(false_label, ctx);
		}

		break;
	}
	case AST_RETURN_STATEMENT: {
		struct return_statement *return_statement = RETURN_STATEMENT(node);
		struct ir_operand *return_value = NULL;
		if (return_statement->return_value) {
			return_value = helper(NODE(return_statement->return_value), ctx);
		}
		struct ret *ret = MAKE_RET();
		ret->return_value = return_value;
		add_instruction(ret, ctx);
		break;
	}
	case AST_WHILE_STATEMENT: {
		struct while_statement *while_statement = WHILE_STATEMENT(node);

		struct label *cond_label = create_label(ctx);
		struct label *true_label = create_label(ctx);
		struct label *false_label = create_label(ctx);

		struct jmp *jmp = MAKE_JMP();
		jmp->jump.target = cond_label;
		add_instruction(jmp, ctx);
		add_instruction(true_label, ctx);
		helper(NODE(while_statement->loop_body), ctx);
		add_instruction(cond_label, ctx);
		emit_code_for_condition(while_statement->condition, true_label, false_label, ctx);
		add_instruction(false_label, ctx);
		break;
	}
	case AST_ARRAY_TYPE_NAME: {
		break;
	}
	case AST_PRIMITIVE_TYPE_NAME: {
		break;
	}
	case AST_FUNCTION_DECLARATION: {
		struct function_declaration *function_declaration = FUNCTION_DECLARATION(node);

		struct ir_function *function = allocate_memory(sizeof(*function));
		function->name = function_declaration->declaration.name;
		array_add(ctx->program->functions, function);

		assert(!ctx->surrounding_function);
		ctx->surrounding_function = function;
		ctx->virtual_register_counter = 0;

		array_fori(function_declaration->parameter_declarations, i) {
			helper(NODE(function_declaration->parameter_declarations[i]), ctx);
		}
		helper(NODE(function_declaration->return_type_name), ctx);
		helper(NODE(function_declaration->function_body), ctx);

		if (array_empty(function->instructions) ||
		    array_last(function->instructions)->class != INSTRUCTION_RET) {
			type_t *return_type = GET_TYPE(function_declaration)->return_type;
			struct ret *ret = MAKE_RET();
			ret->return_value = &make_default_constant(return_type)->operand;
			add_instruction(ret, ctx);
		}

		ctx->surrounding_function = NULL;
		break;
	}
	case AST_PARAMETER_DECLARATION: {
		struct parameter_declaration *parameter_declaration = PARAMETER_DECLARATION(node);
		symbol_t *symbol = GET_SYMBOL(parameter_declaration);
		struct ir_variable *var = make_ir_variable(symbol->name, symbol->type, false);
		symbol->ir_variable = var;
		array_add(ctx->surrounding_function->parameters, var);

		helper(NODE(parameter_declaration->type_name), ctx);
		break;
	}
	case AST_VARIABLE_DECLARATION: {
		struct variable_declaration *variable_declaration = VARIABLE_DECLARATION(node);
		symbol_t *symbol = GET_SYMBOL(variable_declaration);
		if (symbol->class == SYMBOL_GLOBAL_VARIABLE) {
			assert(symbol->ir_variable); // case AST_PROGRAM takes care of these
			break;
		}

		assert(symbol->class == SYMBOL_LOCAL_VARIABLE);
		struct ir_variable *var = make_ir_variable(symbol->name, symbol->type, false);
		symbol->ir_variable = var;
		array_add(ctx->surrounding_function->local_variables, var);

		helper(NODE(variable_declaration->type_name), ctx);
		if (variable_declaration->initializer) {
			struct ir_operand *op = helper(NODE(variable_declaration->initializer), ctx);
			struct mov *mov = MAKE_MOV();
			mov->source_operand = op;
			mov->assignment.target_variable = var;
			add_instruction(mov, ctx);
		}
		break;
	}
	case AST_INVALID:
	default:
		assert(false);
	}

	return retval;
}

struct ir_program *generate_ir(struct program *program)
{
	struct context ctx = {
		.program = NULL,
		.surrounding_function = NULL,
	};
	helper(NODE(program), &ctx);
	return ctx.program;
}

static void dump_constant(struct ir_constant *constant)
{
	if (constant->operand.type == type_int) {
		printf("#%" PRIu64, constant->u64);
	} else {
		assert(constant->operand.type == type_real);
		printf("#%f", constant->f64);
	}
}

static void dump_variable(struct ir_variable *variable)
{
	if (variable->global) {
		putchar('*');
	}
	printf("%s", variable->name->string);
}

static void dump_operand(struct ir_operand *operand)
{
	if (IS_IR_VARIABLE(operand)) {
		dump_variable(IR_VARIABLE(operand));
	} else {
		dump_constant(IR_CONSTANT(operand));
	}
}

static void dump_label(struct label *label)
{
	printf(".L%zu", label->number);
}

static void dump_instruction(struct ir_instruction *instruction)
{
	switch (instruction->class) {
	case INSTRUCTION_NOP: {
		struct nop *nop = NOP(instruction);
		printf("nop\n");
		break;
	}
	case INSTRUCTION_LABEL: {
		struct label *label = LABEL(instruction);
		dump_label(label);
		printf(":\n");
		break;
	}
	case INSTRUCTION_MOV: {
		struct mov *mov = MOV(instruction);
		printf("mov ");
		dump_variable(mov->assignment.target_variable);
		printf(", ");
		dump_operand(mov->source_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_LOAD: {
		struct load *load = LOAD(instruction);
		printf("load ");
		dump_variable(load->assignment.target_variable);
		printf(", ");
		dump_variable(load->array_variable);
		printf(", ");
		dump_operand(load->index);
		putchar('\n');
		break;
	}
	case INSTRUCTION_STORE: {
		struct store *store = STORE(instruction);
		printf("store ");
		dump_variable(store->array_variable);
		printf(", ");
		dump_operand(store->index);
		printf(", ");
		dump_operand(store->source_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_ADD: {
		struct add *add = ADD(instruction);
		printf("add ");
		dump_variable(add->binary_operation.assignment.target_variable);
		printf(", ");
		dump_operand(add->binary_operation.left_operand);
		printf(", ");
		dump_operand(add->binary_operation.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_SUB: {
		struct sub *sub = SUB(instruction);
		printf("sub ");
		dump_variable(sub->binary_operation.assignment.target_variable);
		printf(", ");
		dump_operand(sub->binary_operation.left_operand);
		printf(", ");
		dump_operand(sub->binary_operation.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_MUL: {
		struct mul *mul = MUL(instruction);
		printf("mul ");
		dump_variable(mul->binary_operation.assignment.target_variable);
		printf(", ");
		dump_operand(mul->binary_operation.left_operand);
		printf(", ");
		dump_operand(mul->binary_operation.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_DIV: {
		struct div *div = DIV(instruction);
		printf("div ");
		dump_variable(div->binary_operation.assignment.target_variable);
		printf(", ");
		dump_operand(div->binary_operation.left_operand);
		printf(", ");
		dump_operand(div->binary_operation.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_JMP: {
		struct jmp *jmp = JMP(instruction);
		printf("jmp ");
		dump_label(jmp->jump.target);
		putchar('\n');
		break;
	}
	case INSTRUCTION_JEQ: {
		struct jeq *jeq = JEQ(instruction);
		printf("jeq ");
		dump_label(jeq->conditional_jump.jump.target);
		printf(", ");
		dump_operand(jeq->conditional_jump.left_operand);
		printf(", ");
		dump_operand(jeq->conditional_jump.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_JNE: {
		struct jne *jne = JNE(instruction);
		printf("jne ");
		dump_label(jne->conditional_jump.jump.target);
		printf(", ");
		dump_operand(jne->conditional_jump.left_operand);
		printf(", ");
		dump_operand(jne->conditional_jump.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_JLT: {
		struct jlt *jlt = JLT(instruction);
		printf("jlt ");
		dump_label(jlt->conditional_jump.jump.target);
		printf(", ");
		dump_operand(jlt->conditional_jump.left_operand);
		printf(", ");
		dump_operand(jlt->conditional_jump.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_JLE: {
		struct jle *jle = JLE(instruction);
		printf("jle ");
		dump_label(jle->conditional_jump.jump.target);
		printf(", ");
		dump_operand(jle->conditional_jump.left_operand);
		printf(", ");
		dump_operand(jle->conditional_jump.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_JGT: {
		struct jgt *jgt = JGT(instruction);
		printf("jgt ");
		dump_label(jgt->conditional_jump.jump.target);
		printf(", ");
		dump_operand(jgt->conditional_jump.left_operand);
		printf(", ");
		dump_operand(jgt->conditional_jump.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_JGE: {
		struct jge *jge = JGE(instruction);
		printf("jge ");
		dump_label(jge->conditional_jump.jump.target);
		printf(", ");
		dump_operand(jge->conditional_jump.left_operand);
		printf(", ");
		dump_operand(jge->conditional_jump.right_operand);
		putchar('\n');
		break;
	}
	case INSTRUCTION_CALL: {
		struct call *call = CALL(instruction);
		printf("call %s", call->callee->string);
		array_fori(call->arguments, i) {
			printf(", ");
			dump_operand(call->arguments[i]);
		}
		putchar('\n');
		break;
	}
	case INSTRUCTION_RET: {
		struct ret *ret = RET(instruction);
		printf("ret");
		if (ret->return_value) {
			putchar(' ');
			dump_operand(ret->return_value);
		}
		putchar('\n');
		break;
	}
	case INSTRUCTION_I2R: {
		struct i2r *i2r = I2R(instruction);
		printf("i2r ");
		dump_variable(i2r->cast_operation.assignment.target_variable);
		printf(", ");
		dump_operand(i2r->cast_operation.casted_operand);
		break;
	}
	case INSTRUCTION_R2I: {
		struct r2i *r2i = R2I(instruction);
		printf("r2i ");
		dump_variable(r2i->cast_operation.assignment.target_variable);
		printf(", ");
		dump_operand(r2i->cast_operation.casted_operand);
		break;
	}
	case INSTRUCTION_INVALID:
	default:
		assert(false);
	}
}

static void dump_function(struct ir_function *function)
{
	printf("%s:\n", function->name->string);
	array_fori(function->instructions, i) {
		dump_instruction(function->instructions[i]);
	}
}

void dump_ir(struct ir_program *program)
{
	array_fori(program->global_variables, i) {
		printf("var ");
		dump_variable(program->global_variables[i]);
		putchar('\n');
	}

	array_fori(program->functions, i) {
		putchar('\n');
		dump_function(program->functions[i]);
	}
}

/*
	switch (instruction->class) {
	case INSTRUCTION_NOP: {
		struct nop *nop = NOP(instruction);
		break;
	}
	case INSTRUCTION_LABEL: {
		struct label *label = LABEL(instruction);
		break;
	}
	case INSTRUCTION_MOV: {
		struct mov *mov = MOV(instruction);
		break;
	}
	case INSTRUCTION_LOAD: {
		struct load *load = LOAD(instruction);
		break;
	}
	case INSTRUCTION_STORE: {
		struct store *store = STORE(instruction);
		break;
	}
	case INSTRUCTION_ADD: {
		struct add *add = ADD(instruction);
		break;
	}
	case INSTRUCTION_SUB: {
		struct sub *sub = SUB(instruction);
		break;
	}
	case INSTRUCTION_MUL: {
		struct mul *mul = MUL(instruction);
		break;
	}
	case INSTRUCTION_DIV: {
		struct div *div = DIV(instruction);
		break;
	}
	case INSTRUCTION_JMP: {
		struct jmp *jmp = JMP(instruction);
		break;
	}
	case INSTRUCTION_JEQ: {
		struct jeq *jeq = JEQ(instruction);
		break;
	}
	case INSTRUCTION_JNE: {
		struct jne *jne = JNE(instruction);
		break;
	}
	case INSTRUCTION_JLT: {
		struct jlt *jlt = JLT(instruction);
		break;
	}
	case INSTRUCTION_JLE: {
		struct jle *jle = JLE(instruction);
		break;
	}
	case INSTRUCTION_JGT: {
		struct jgt *jgt = JGT(instruction);
		break;
	}
	case INSTRUCTION_JGE: {
		struct jge *jge = JGE(instruction);
		break;
	}
	case INSTRUCTION_CALL: {
		struct call *call = CALL(instruction);
		break;
	}
	case INSTRUCTION_RET: {
		struct ret *ret = RET(instruction);
		break;
	}
	case INSTRUCTION_I2R: {
		struct i2r *i2r = I2R(instruction);
		break;
	}
	case INSTRUCTION_R2I: {
		struct r2i *r2i = R2I(instruction);
		break;
	}
	case INSTRUCTION_INVALID:
	default:
		assert(false);
	}
*/
