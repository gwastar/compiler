#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include "array.h"
#include "dbuf.h"
#include "error.h"
#include "tokenizer.h"

struct tokenizer {
	srcfile_t *file;
	token_t *tokens;
	size_t line;
	size_t column;
	size_t index;
};

struct keyword {
	const char *name;
	size_t namelen;
	enum token_type token_type;
};

#define KEYWORD(name, token_type) {#name, sizeof(#name) - 1, token_type}
static const struct keyword keywords[] = {
	KEYWORD(var, TOKEN_VAR),
	KEYWORD(func, TOKEN_FUNC),
	KEYWORD(end, TOKEN_END),
	KEYWORD(if, TOKEN_IF),
	KEYWORD(then, TOKEN_THEN),
	KEYWORD(else, TOKEN_ELSE),
	KEYWORD(while, TOKEN_WHILE),
	KEYWORD(do, TOKEN_DO),
	KEYWORD(return, TOKEN_RETURN),
	KEYWORD(or, TOKEN_OR),
	KEYWORD(and, TOKEN_AND),
	KEYWORD(as, TOKEN_AS),
	KEYWORD(int, TOKEN_INT),
	KEYWORD(real, TOKEN_REAL),
};

static int pop(struct tokenizer *tokenizer)
{
	int c = getc(tokenizer->file->handle);
	error_on(ferror(tokenizer->file->handle), "Failed to read from '%s'", tokenizer->file->path);
	tokenizer->file->buf[tokenizer->index++] = c;
	if (c == '\n') {
		tokenizer->line++;
		tokenizer->column = 1;
	} else {
		tokenizer->column++;
	}
	return c;
}

static void pop_expected(int expected, struct tokenizer *tokenizer)
{
	size_t line = tokenizer->line;
	size_t column = tokenizer->column;
	int c = pop(tokenizer);
	if (c != expected) {
		// TODO print EOF correctly
		fprintf(stderr, "%s:%zu:%zu: error: expected '%c', got '%c'\n",
			tokenizer->file->path, line, column, expected, c);
		exit(EXIT_FAILURE);
	}
}

static int peek(struct tokenizer *tokenizer)
{
	int c = getc(tokenizer->file->handle);
	error_on(ferror(tokenizer->file->handle), "Failed to read from '%s'", tokenizer->file->path);
	int rv = ungetc(c, tokenizer->file->handle);
	error_on(rv != c, "ungetc failed");
	return c;
}

static void add_simple_token(enum token_type type, size_t line, size_t column, struct tokenizer *tokenizer)
{
	token_t token = {
		.type = type,
		.loc = make_loc(tokenizer->file, line, column, line, column),
	};
	array_add(tokenizer->tokens, token);
}

static void add_compound_token(enum token_type type, size_t line, size_t column, struct tokenizer *tokenizer)
{
	token_t token = {
		.type = type,
		.loc = make_loc(tokenizer->file, line, column, line, column + 1),
	};
	array_add(tokenizer->tokens, token);
}

static bool is_start_of_ident(int c)
{
	return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || (c == '_');
}

static bool is_part_of_ident(int c)
{
	return is_start_of_ident(c) || ('0' <= c && c <= '9');
}

static srcfile_t *open_source_file(const char *filepath)
{
	FILE *handle = fopen(filepath, "r");
	error_on(!handle, "unable to open source file '%s'", filepath);
	int rv = fseek(handle, 0, SEEK_END);
	error_on(rv == -1, "fseek failed");
	long size = ftell(handle);
	error_on(size < 0, "ftell failed");
	rewind(handle);
	srcfile_t *file = allocate_memory(sizeof(*file) + size + 1);
	file->buf[size] = '\0';
	file->path = filepath;
	file->size = size;
	file->handle = handle;
	return file;
}

token_t *tokenize(const char *filepath)
{

	struct tokenizer tokenizer = {
		.file = open_source_file(filepath),
		.tokens = NULL,
		.line = 1,
		.column = 1,
		.index = 0,
	};

	struct dbuf buf;
	dbuf_init(&buf);

	while (!feof(tokenizer.file->handle)) {
		size_t line = tokenizer.line;
		size_t column = tokenizer.column;
		int c = pop(&tokenizer);
		switch (c) {
		case EOF:
			add_simple_token(TOKEN_EOF, line, column, &tokenizer);
			continue;
		case ';':
			add_simple_token(';', line, column, &tokenizer);
			continue;
		case ':':
			if (peek(&tokenizer) == '=') {
				c = pop(&tokenizer);
				add_compound_token(TOKEN_ASSIGN, line, column, &tokenizer);
			} else {
				add_simple_token(':', line, column, &tokenizer);
			}
			continue;
		case ',':
			add_simple_token(',', line, column, &tokenizer);
			continue;
		case '(':
			add_simple_token('(', line, column, &tokenizer);
			continue;
		case ')':
			add_simple_token(')', line, column, &tokenizer);
			continue;
		case '[':
			add_simple_token('[', line, column, &tokenizer);
			continue;
		case ']':
			add_simple_token(']', line, column, &tokenizer);
			continue;
		case '+':
			add_simple_token('+', line, column, &tokenizer);
			continue;
		case '-':
			add_simple_token('-', line, column, &tokenizer);
			continue;
		case '*':
			add_simple_token('*', line, column, &tokenizer);
			continue;
		case '/':
			add_simple_token('/', line, column, &tokenizer);
			continue;
		case '=':
			pop_expected('=', &tokenizer);
			add_compound_token(TOKEN_EQUAL, line, column, &tokenizer);
			continue;
		case '!':
			pop_expected('=', &tokenizer);
			add_compound_token(TOKEN_UNEQUAL, line, column, &tokenizer);
			continue;
		case '<':
			if (peek(&tokenizer) == '=') {
				c = pop(&tokenizer);
				add_compound_token(TOKEN_LESSEQUAL, line, column, &tokenizer);
			} else {
				add_compound_token(TOKEN_LESSTHAN, line, column, &tokenizer);
			}
			continue;
		case '>':
			if (peek(&tokenizer) == '=') {
				c = pop(&tokenizer);
				add_compound_token(TOKEN_GREATEREQUAL, line, column, &tokenizer);
			} else {
				add_compound_token(TOKEN_GREATERTHAN, line, column, &tokenizer);
			}
			continue;
		case ' ':
		case '\t':
		case '\r':
		case '\n':
			continue;
		case '#':
			do {
				if (peek(&tokenizer) == EOF) {
					continue;
				}
				c = pop(&tokenizer);
			} while (c != '\n');
			continue;
		default:
			break;
		}

		if (c == '\'') {
			c = pop(&tokenizer);
			if (c == '\'') {
				fprintf(stderr, "%s:%zu:%zu: error: empty character literal\n",
					tokenizer.file->path, line, column);
				exit(EXIT_FAILURE);
			}

			if (c == '\n') {
				fprintf(stderr, "%s:%zu:%zu: error: newline in character literal\n",
					tokenizer.file->path, line, column);
				exit(EXIT_FAILURE);
			}

			if (c == '\\') {
				c = pop(&tokenizer);
				switch (c) {
				case '\\': c = '\\'; break;
				case '\'': c = '\''; break;
				case 'n': c = '\n'; break;
				case 'r': c = '\r'; break;
				case 't': c = '\t'; break;
				case '0': c = '\0'; break;
				case EOF: c = EOF; break; // will error below
				default:
					fprintf(stderr, "%s:%zu:%zu: error: unknown escape character '\\%c'\n",
						tokenizer.file->path, line, column + 2, c);
					exit(EXIT_FAILURE);
				}
			}

			if (c == EOF) {
				fprintf(stderr, "%s:%zu:%zu: error: EOF in character literal\n",
					tokenizer.file->path, line, column);
				exit(EXIT_FAILURE);
			}

			pop_expected('\'', &tokenizer);
			token_t token = {
				.type = TOKEN_CHARLITERAL,
				.ch = c,
				.loc = make_loc(tokenizer.file, line, column,
						tokenizer.line, tokenizer.column - 1),
			};
			array_add(tokenizer.tokens, token);
			continue;
		}

		if ('0' <= c && c <= '9') {
			dbuf_addb(&buf, c);
			bool found_dot = false;
			for (;;) {
				int next = peek(&tokenizer);
				if ('0' <= next && next <= '9') {
					c = pop(&tokenizer);
					dbuf_addb(&buf, c);
					continue;
				}
				if (next == '.' && !found_dot) {
					c = pop(&tokenizer);
					dbuf_addb(&buf, c);
					found_dot = true;
					continue;
				}

				break;
			}

			if (c == '.') {
				fprintf(stderr, "%s:%zu:%zu: error: expected digit after '.'\n",
					tokenizer.file->path, tokenizer.line, tokenizer.column);
				exit(EXIT_FAILURE);
			}

			dbuf_addb(&buf, '\0');

			token_t token = { .type = TOKEN_INVALID };
			errno = 0;
			if (found_dot) {
				char *endptr;
				double f64 = strtod(dbuf_buffer(&buf), &endptr);
				assert(*endptr == '\0');
				if (errno == ERANGE) {
					fprintf(stderr, "%s:%zu:%zu: error: Floating point literal caused %s\n",
						tokenizer.file->path, line, column,
						f64 == 0 ? "underflow" : "overflow");
				}
				token.type = TOKEN_FLOATLITERAL;
				token.f64 = f64;
				token.loc = make_loc(tokenizer.file, line, column,
						     tokenizer.line, tokenizer.column - 1);
			} else {
				char *endptr;
				uint64_t u64 = strtoull(dbuf_buffer(&buf), &endptr, 10);
				assert(*endptr == '\0');
				if (errno == ERANGE) {
					fprintf(stderr, "%s:%zu:%zu: error: Number literal is too big\n",
						tokenizer.file->path, line, column);
				}
				token.type = TOKEN_INTLITERAL;
				token.u64 = u64;
				token.loc = make_loc(tokenizer.file, line, column,
						     tokenizer.line, tokenizer.column - 1);
			}
			array_add(tokenizer.tokens, token);

			dbuf_reset(&buf);
			continue;
		}

		if (is_start_of_ident(c)) {
			dbuf_addb(&buf, c);

			for (;;) {
				int next = peek(&tokenizer);
				if (!is_part_of_ident(next)) {
					break;
				}
				c = pop(&tokenizer);
				dbuf_addb(&buf, c);
			}

			size_t len = dbuf_size(&buf);
			dbuf_addb(&buf, '\0');
			const char *s = dbuf_buffer(&buf);

			bool found_keyword = false;
			for (size_t i = 0; i < (sizeof(keywords) / sizeof(keywords[0])); i++) {
				if (len != keywords[i].namelen) {
					continue;
				}
				if (memcmp(s, keywords[i].name, len) != 0) {
					continue;
				}
				token_t token = {
					.type = keywords[i].token_type,
					.loc = make_loc(tokenizer.file, line, column,
							tokenizer.line, tokenizer.column - 1),
				};
				array_add(tokenizer.tokens, token);
				found_keyword = true;
				break;
			}
			if (found_keyword) {
				dbuf_reset(&buf);
				continue;
			}

			token_t token = {
				.type = TOKEN_IDENT,
				.atom = make_atom(dbuf_finalize(&buf), len),
				.loc = make_loc(tokenizer.file, line, column,
						tokenizer.line, tokenizer.column - 1),
			};
			array_add(tokenizer.tokens, token);

			continue;
		}

		fprintf(stderr, "%s:%zu:%zu: error: Unexpected character '%c'\n",
			tokenizer.file->path, line, column, c);
	}

	return array_empty(tokenizer.tokens) ? NULL : tokenizer.tokens;
}

void dump_tokens(token_t *tokens)
{
	token_t *token;
	array_foreach(tokens, token) {
		int n;
		switch (token->type) {
		case TOKEN_EOF:
			n = printf("EOF");
			break;
		case TOKEN_CHARLITERAL:
			n = printf("CHAR(%c)", token->ch);
			break;
		case TOKEN_INTLITERAL:
			n = printf("INT(%llu)", (unsigned long long)token->u64);
			break;
		case TOKEN_FLOATLITERAL:
			n = printf("FLOAT(%f)", token->f64);
			break;
		case TOKEN_IDENT:
			n = printf("IDENT(%s)", token->atom->string);
			break;
		case TOKEN_VAR:
			n = printf("var");
			break;
		case TOKEN_FUNC:
			n = printf("func");
			break;
		case TOKEN_END:
			n = printf("end");
			break;
		case TOKEN_IF:
			n = printf("if");
			break;
		case TOKEN_THEN:
			n = printf("then");
			break;
		case TOKEN_ELSE:
			n = printf("else");
			break;
		case TOKEN_WHILE:
			n = printf("while");
			break;
		case TOKEN_DO:
			n = printf("do");
			break;
		case TOKEN_RETURN:
			n = printf("return");
			break;
		case TOKEN_OR:
			n = printf("or");
			break;
		case TOKEN_AND:
			n = printf("and");
			break;
		case TOKEN_AS:
			n = printf("as");
			break;
		case TOKEN_EQUAL:
			n = printf("==");
			break;
		case TOKEN_UNEQUAL:
			n = printf("!=");
			break;
		case TOKEN_LESSTHAN:
			n = printf("<");
			break;
		case TOKEN_LESSEQUAL:
			n = printf("<=");
			break;
		case TOKEN_GREATERTHAN:
			n = printf(">");
			break;
		case TOKEN_GREATEREQUAL:
			n = printf(">=");
			break;
		case TOKEN_ASSIGN:
			n = printf(":=");
			break;
		case TOKEN_SEMICOLON:
			n = printf(";");
			break;
		case TOKEN_COLON:
			n = printf(":");
			break;
		case TOKEN_COMMA:
			n = printf(",");
			break;
		case TOKEN_OPENPAREN:
			n = printf("(");
			break;
		case TOKEN_CLOSEPAREN:
			n = printf(")");
			break;
		case TOKEN_OPENBRACKET:
			n = printf("[");
			break;
		case TOKEN_CLOSEBRACKET:
			n = printf("]");
			break;
		case TOKEN_PLUS:
			n = printf("+");
			break;
		case TOKEN_MINUS:
			n = printf("-");
			break;
		case TOKEN_ASTERISK:
			n = printf("*");
			break;
		case TOKEN_SLASH:
			n = printf("/");
			break;
		case TOKEN_INVALID:
		default:
			n = printf("INVALID");
		}

		while (n < 30) {
			putchar(' ');
			n++;
		}

		printf("(%s: %zu:%zu -- %zu:%zu)", token->loc.file->path, token->loc.start.line,
		       token->loc.start.column, token->loc.end.line, token->loc.end.column);

		putchar('\n');
	}
}
