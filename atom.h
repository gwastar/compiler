#ifndef __IDENTIFIER_INCLUDE__
#define __IDENTIFIER_INCLUDE__

#include <stdint.h>

typedef struct atom {
	const char *string;
	size_t len;
	uint64_t hash;
} atom_t;

void init_global_atom_table(void);
atom_t *make_atom(const char *name, size_t len); // takes ownership of name! @Cleanup or should it make a copy?

#endif
