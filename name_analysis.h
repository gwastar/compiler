#ifndef __NAME_ANALYSYS_INCLUDE__
#define __NAME_ANALYSYS_INCLUDE__

#include "ast.h"

void perform_name_analysis(struct program *program);

#endif
