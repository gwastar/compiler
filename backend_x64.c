#include <inttypes.h>
#include <stdlib.h>
#include "array.h"
#include "backend_x64.h"
#include "dbuf.h"
#include "error.h"
#include "mprintf.h"
#include "typer.h"

static uint64_t get_size_for_type(type_t *type)
{
	if (type->class == TYPE_PRIMITIVE) {
		if (type == type_void) {
			assert(false);
			return 0;
		}
		return 8;
	}

	if (type->class == TYPE_ARRAY) {
		uint64_t size = get_size_for_type(type->base_type);
		array_fori(type->lengths, i) {
			size *= type->lengths[i];
		}
		return size;
	}

	assert(false);
	return 0;
}

static void advance_stack_offset(int64_t *poffset, type_t *type) {
	*poffset = (*poffset + get_size_for_type(type) + 7) & ~7;
}

static void _load(const char *instr, const char *dest_reg, struct ir_operand *op, FILE *file)
{
	fprintf(file, "%s %s, ", instr, dest_reg);
	if (IS_IR_CONSTANT(op)) {
		struct ir_constant *constant = IR_CONSTANT(op);
		assert(op->type == type_int);
		fprintf(file, "%" PRIu64 "\n", constant->u64);
		return;
	}

	struct ir_variable *var = IR_VARIABLE(op);
	if (var->global) {
		fprintf(file, "%s\n", var->name->string);
		return;
	}
	int64_t rbp_offset = -var->stack_offset - get_size_for_type(var->operand.type);
	fputs("[rbp", file);
	if (rbp_offset < 0) {
		fprintf(file, " - %" PRIu64, -rbp_offset);
	} else if (rbp_offset > 0) {
		fprintf(file, " + %" PRIu64, rbp_offset);
	}
	fputs("]\n", file);
}

static void load(const char *dest_reg, struct ir_operand *op, FILE *file)
{
	assert(strlen(dest_reg) == 3 && dest_reg[0] == 'r');
	assert(op->type == type_int);
	_load("mov", dest_reg, op, file);
}

static void loadf(const char *dest_reg, struct ir_operand *op, FILE *file)
{
	assert(strlen(dest_reg) == 4 && memcmp(dest_reg, "xmm", 3) == 0);
	assert(op->type == type_real);

	if (IS_IR_CONSTANT(op)) {
		// @Cleanup quick hack
		struct ir_constant *constant = IR_CONSTANT(op);
		assert(op->type == type_real);
		fputs("push rdx\n", file);
		fprintf(file, "mov rdx, 0x%" PRIx64 "\n", constant->u64);
		fprintf(file, "movq %s, rdx\n", dest_reg);
		fputs("pop rdx\n", file);
		return;
	}
	_load("movsd", dest_reg, op, file);
}


static void load_address(const char *dest_reg, struct ir_variable *var, FILE *file)
{
	if (var->global) {
		fprintf(file, "lea %s, %s\n", dest_reg, var->name->string);
		return;
	}
	int64_t rbp_offset = -var->stack_offset - get_size_for_type(var->operand.type);
	fprintf(file, "lea %s, [rbp", dest_reg);
	if (rbp_offset < 0) {
		fprintf(file, " - %" PRIu64, -rbp_offset);
	} else if (rbp_offset > 0) {
		fprintf(file, " + %" PRIu64, rbp_offset);
	}
	fputs("]\n", file);
}

static void _store(const char *instr, struct ir_variable *var, const char *src_reg,  FILE *file)
{
	fprintf(file, "%s ", instr);
	if (var->global) {
		fprintf(file, "%s", var->name->string);
	} else {
		int64_t rbp_offset = -var->stack_offset - get_size_for_type(var->operand.type);
		fputs("[rbp", file);
		if (rbp_offset < 0) {
			fprintf(file, " - %" PRIu64, -rbp_offset);
		} else if (rbp_offset > 0) {
			fprintf(file, " + %" PRIu64, rbp_offset);
		}
		fputs("]", file);
	}

	fprintf(file, ", %s\n", src_reg);
}

static void store(struct ir_variable *var, const char *src_reg,  FILE *file)
{
	assert(strlen(src_reg) == 3 && src_reg[0] == 'r');
	assert(var->operand.type == type_int);
	_store("mov", var, src_reg, file);
}

static void storef(struct ir_variable *var, const char *src_reg,  FILE *file)
{
	assert(strlen(src_reg) == 4 && memcmp(src_reg, "xmm", 3) == 0);
	assert(var->operand.type == type_real);
	_store("movsd", var, src_reg, file);
}

static void emit_cmp(struct conditional_jump *cjmp, FILE *file)
{
	struct ir_operand *left = cjmp->left_operand;
	struct ir_operand *right = cjmp->right_operand;
	if (left->type == type_int) {
		load("rax", left, file);
		load("rcx", right, file);
		fputs("cmp rax, rcx\n", file);
	} else {
		loadf("xmm0", left, file);
		loadf("xmm1", right, file);
		fputs("ucomisd xmm0, xmm1\n", file);
	}
}

static void generate_instruction(struct ir_instruction *instruction, FILE *file)
{
	switch (instruction->class) {
	case INSTRUCTION_NOP: {
		struct nop *nop = NOP(instruction);
		// fputs(file, "nop\n");
		break;
	}
	case INSTRUCTION_LABEL: {
		struct label *label = LABEL(instruction);
		fprintf(file, ".L%zu:\n", label->number);
		break;
	}
	case INSTRUCTION_MOV: {
		struct mov *mov = MOV(instruction);
		type_t *type = mov->source_operand->type;
		assert(mov->assignment.target_variable->operand.type == type);
		if (type == type_int) {
			load("rax", mov->source_operand, file);
			store(mov->assignment.target_variable, "rax", file);
		} else {
			loadf("xmm0", mov->source_operand, file);
			storef(mov->assignment.target_variable, "xmm0", file);
		}
		break;
	}
	case INSTRUCTION_LOAD: {
		struct load *ld = LOAD(instruction);
		load_address("rsi", ld->array_variable, file);
		load("rcx", ld->index, file);
		type_t *type = ld->array_variable->operand.type->base_type;
		assert(ld->assignment.target_variable->operand.type == type);
		assert(get_size_for_type(type) == 8);
		if (type == type_int) {
			fputs("mov rax, [rsi + 8 * rcx]\n", file);
			store(ld->assignment.target_variable, "rax", file);
		} else {
			fputs("movsd xmm0, [rsi + 8 * rcx]\n", file);
			storef(ld->assignment.target_variable, "xmm0", file);
		}
		break;
	}
	case INSTRUCTION_STORE: {
		struct store *st = STORE(instruction);
		load_address("rdi", st->array_variable, file);
		load("rcx", st->index, file);
		type_t *type = st->array_variable->operand.type->base_type;
		assert(st->source_operand->type == type);
		assert(get_size_for_type(type) == 8);
		if (type == type_int) {
			load("rax", st->source_operand, file);
			fputs("mov [rdi + 8 * rcx], rax\n", file);
		} else {
			loadf("xmm0", st->source_operand, file);
			fputs("movsd [rdi + 8 * rcx], xmm0\n", file);
		}
		break;
	}
	case INSTRUCTION_ADD:
	case INSTRUCTION_SUB:
	case INSTRUCTION_MUL:
	case INSTRUCTION_DIV: {
		struct binary_operation *binop = BINARY_OPERATION(instruction);
		struct ir_operand *left = binop->left_operand;
		struct ir_operand *right = binop->right_operand;
		struct ir_variable *target = binop->assignment.target_variable;
		type_t *type = left->type;
		assert(right->type == type);
		assert(target->operand.type == type);
		if (type == type_int) {
			load("rax", left, file);
			load("rcx", right, file);
			switch (instruction->class) {
			case INSTRUCTION_ADD: fputs("add rax, rcx\n", file); break;
			case INSTRUCTION_SUB: fputs("sub rax, rcx\n", file); break;
			case INSTRUCTION_MUL: fputs("imul rax, rcx\n", file); break;
			case INSTRUCTION_DIV: fputs("cqo\n" "idiv rcx\n", file); break;
			default: assert(false); break;
			}
			store(target, "rax", file);
		} else {
			loadf("xmm0", left, file);
			loadf("xmm1", right, file);
			switch (instruction->class) {
			case INSTRUCTION_ADD: fputs("addsd xmm0, xmm1\n", file); break;
			case INSTRUCTION_SUB: fputs("subsd xmm0, xmm1\n", file); break;
			case INSTRUCTION_MUL: fputs("mulsd xmm0, xmm1\n", file); break;
			case INSTRUCTION_DIV: fputs("divsd xmm0, xmm1\n", file); break;
			default: assert(false); break;
			}
			storef(target, "xmm0", file);
		}
		break;
	}
	case INSTRUCTION_JMP: {
		struct jmp *jmp = JMP(instruction);
		fprintf(file, "jmp .L%zu\n", jmp->jump.target->number);
		break;
	}
	case INSTRUCTION_JEQ: {
		struct jeq *jeq = JEQ(instruction);
		emit_cmp(&jeq->conditional_jump, file);
		fprintf(file, "je .L%zu\n", jeq->conditional_jump.jump.target->number);
		break;
	}
	case INSTRUCTION_JNE: {
		struct jne *jne = JNE(instruction);
		emit_cmp(&jne->conditional_jump, file);
		fprintf(file, "jne .L%zu\n", jne->conditional_jump.jump.target->number);
		break;
	}
	case INSTRUCTION_JLT: {
		struct jlt *jlt = JLT(instruction);
		emit_cmp(&jlt->conditional_jump, file);
		fprintf(file, "jl .L%zu\n", jlt->conditional_jump.jump.target->number);
		break;
	}
	case INSTRUCTION_JLE: {
		struct jle *jle = JLE(instruction);
		emit_cmp(&jle->conditional_jump, file);
		fprintf(file, "jle .L%zu\n", jle->conditional_jump.jump.target->number);
		break;
	}
	case INSTRUCTION_JGT: {
		struct jgt *jgt = JGT(instruction);
		emit_cmp(&jgt->conditional_jump, file);
		fprintf(file, "jg .L%zu\n", jgt->conditional_jump.jump.target->number);
		break;
	}
	case INSTRUCTION_JGE: {
		struct jge *jge = JGE(instruction);
		emit_cmp(&jge->conditional_jump, file);
		fprintf(file, "jge .L%zu\n", jge->conditional_jump.jump.target->number);
		break;
	}
	case INSTRUCTION_CALL: {
		struct call *call = CALL(instruction);
		array_fori(call->arguments, i) {
			if (call->arguments[i]->type == type_int) {
				load("rax", call->arguments[i], file);
				fputs("push rax\n", file);
			} else {
				loadf("xmm0", call->arguments[i], file);
				fputs("sub rsp, 8\n", file);
				fputs("movq [rsp], xmm0\n", file);
			}
		}
		fprintf(file, "call %s\n", call->callee->string);
		if (!array_empty(call->arguments)) {
			fprintf(file, "add rsp, %" PRIu64 "\n", (uint64_t)8 * array_len(call->arguments));
		}
		struct ir_variable *target = call->assignment.target_variable;
		if (target) {
			if (target->operand.type == type_int) {
				store(target, "rax", file);
			} else {
				storef(target, "xmm0", file);
			}
		}
		break;
	}
	case INSTRUCTION_RET: {
		struct ret *ret = RET(instruction);
		if (ret->return_value) {
			if (ret->return_value->type == type_int) {
				load("rax", ret->return_value, file);
			} else {
				loadf("xmm0", ret->return_value, file);
			}
		}
		fputs("leave\n", file);
		fputs("ret\n", file);
		break;
	}
	case INSTRUCTION_I2R: {
		struct i2r *i2r = I2R(instruction);
		load("rax", i2r->cast_operation.casted_operand, file);
		fputs("cvtsi2sd xmm0, rax\n", file);
		storef(i2r->cast_operation.assignment.target_variable, "xmm0", file);
		break;
	}
	case INSTRUCTION_R2I: {
		struct r2i *r2i = R2I(instruction);
		loadf("xmm0", r2i->cast_operation.casted_operand, file);
		fputs("cvtsd2si rax, xmm0\n", file);
		store(r2i->cast_operation.assignment.target_variable, "rax", file);
		break;
	}
	case INSTRUCTION_INVALID:
	default:
		assert(false);
	}
}

static void generate_function(struct ir_function *function, FILE *file)
{
	int64_t stack_offset = -(16 + 8 * array_len(function->parameters));
	array_fori(function->parameters, i) {
		struct ir_variable *param = function->parameters[i];
		assert(get_size_for_type(param->operand.type) == 8);
		param->stack_offset = stack_offset;
		advance_stack_offset(&stack_offset, param->operand.type);
	}
	stack_offset += 16; // base pointer and return address
	array_fori(function->local_variables, i) {
		struct ir_variable *var = function->local_variables[i];
		var->stack_offset = stack_offset;
		advance_stack_offset(&stack_offset, var->operand.type);
	}
	array_fori(function->virtual_registers, i) {
		struct ir_variable *reg = function->virtual_registers[i];
		reg->stack_offset = stack_offset;
		advance_stack_offset(&stack_offset, reg->operand.type);
	}
	assert(stack_offset >= 0);
	uint64_t stack_frame_size = stack_offset;

	fputs(".align 16\n", file);
	fprintf(file, "%s:\n", function->name->string);
	fputs("push rbp\n", file);
	fputs("mov rbp, rsp\n", file);
	fprintf(file, "sub rsp, %" PRIu64 "\n", stack_frame_size);

	array_fori(function->instructions, i) {
		generate_instruction(function->instructions[i], file);
	}
}

void generate_code(struct ir_program *program, const char *output_file_name)
{
	FILE *file = fopen(output_file_name, "w");
	error_on(!file, "unable to open file '%s' for writing", output_file_name);

	fputs(".intel_syntax noprefix\n", file);
	fputs(".global main\n", file);
	fputc('\n', file);

	array_fori(program->global_variables, i) {
		struct ir_variable *var = program->global_variables[i];
		fprintf(file, ".lcomm %s, %" PRIu64 "\n",
			var->name->string, get_size_for_type(var->operand.type));
	}

	fputs("\n.text\n", file);

	array_fori(program->functions, i) {
		fputc('\n', file);
		generate_function(program->functions[i], file);
	}

	int err = fflush(file);
	error_on(err, "fflush failed");
	fclose(file);
}

void run_assembler(const char *input_file_name, const char *output_file_name)
{
	char *cmd = mprintf("as '%s' -o '%s'", input_file_name, output_file_name);
	int rv = system(cmd);
	error_on(rv == -1, "failed to run assembler");
	if (rv != 0) {
		exit(EXIT_FAILURE);
	}
	free(cmd);
}

void run_linker(const char **input_file_names, const char *output_file_name)
{
	struct dbuf buf;
	dbuf_init(&buf);
	dbuf_addstr(&buf, "ld ");
	array_fori(input_file_names, i) {
		dbuf_addb(&buf, '\'');
		dbuf_addstr(&buf, input_file_names[i]);
		dbuf_addb(&buf, '\'');
		dbuf_addb(&buf, ' ');
	}
	dbuf_addstr(&buf, "runtime_library/e2lib.a"); // TODO prepend compiler path
	dbuf_addstr(&buf, " -o ");
	dbuf_addb(&buf, '\'');
	dbuf_addstr(&buf, output_file_name);
	dbuf_addb(&buf, '\'');
	dbuf_addb(&buf, '\0');

	char *cmd = dbuf_finalize(&buf);
	// puts(cmd);
	int rv = system(cmd);
	error_on(rv == -1, "failed to run linker");
	if (rv != 0) {
		exit(EXIT_FAILURE);
	}
	free(cmd);
}
